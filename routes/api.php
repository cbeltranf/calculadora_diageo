<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {
    Route::group(['prefix' => 'auth'], function () {
        Route::post('login', 'Auth\AuthController@login');
        Route::post('register', 'Auth\AuthController@register');
        Route::get('logout', 'Auth\AuthController@logout');
        Route::get('me', 'Auth\AuthController@me');
    });
    Route::get('strata', 'StratumController@index');
    // City
    Route::get('cities', 'CityController@index');
    // Segment
    Route::get('segments', 'SegmentController@index');
    Route::post('segments', 'SegmentController@store');
    Route::get('segments/{segment}', 'SegmentController@show');
    Route::put('segments/{segment}', 'SegmentController@update');
    Route::delete('segments/{segment}', 'SegmentController@destroy');
    // Establishment
    Route::get('establishments', 'EstablishmentController@index');
    Route::post('establishments', 'EstablishmentController@store');
    Route::get('establishments/{establishment}', 'EstablishmentController@show');
    Route::put('establishments/{establishment}', 'EstablishmentController@update');
    Route::delete('establishments/{establishment}', 'EstablishmentController@destroy');
    Route::get('establishmentsSearch', 'EstablishmentController@search');
    // VolumeSource
    Route::get('volumeSources', 'VolumeSourceController@index');
    Route::post('volumeSources', 'VolumeSourceController@store');
    Route::get('volumeSources/{volumeSource}', 'VolumeSourceController@show');
    Route::put('volumeSources/{volumeSource}', 'VolumeSourceController@update');
    Route::delete('volumeSources/{volumeSource}', 'VolumeSourceController@destroy');
    // Brand
    Route::get('brands', 'BrandController@index');
    Route::post('brands', 'BrandController@store');
    Route::get('brands/{brand}', 'BrandController@show');
    Route::put('brands/{brand}', 'BrandController@update');
    Route::delete('brands/{brand}', 'BrandController@destroy');

    //tax
    Route::resource('tax', 'TaxController');

    // MeasurementUnit
    Route::get('measurementUnits', 'MeasurementUnitController@index');
    Route::post('measurementUnits', 'MeasurementUnitController@store');
    Route::get('measurementUnits/{measurementUnits}', 'MeasurementUnitController@show');
    Route::put('measurementUnits/{measurementUnits}', 'MeasurementUnitController@update');
    Route::delete('measurementUnits/{measurementUnits}', 'MeasurementUnitController@destroy');
    // Category
    /*
    Route::get('categories', 'CategoryController@index');
    Route::post('categories', 'CategoryController@store');
    Route::get('categories/{category}', 'CategoryController@show');
    Route::put('categories/{category}', 'CategoryController@update');
    Route::delete('categories/{category}', 'CategoryController@destroy');
    */
    // Product
    Route::get('products', 'ProductController@index');
    Route::post('products', 'ProductController@store');
    Route::get('products/{product}', 'ProductController@show');
    Route::put('products/{product}', 'ProductController@update');
    Route::delete('products/{product}', 'ProductController@destroy');
    // Ingredient
    Route::get('ingredientsProduct/{product}', 'IngredientController@indexProduct');
    Route::post('ingredients', 'IngredientController@store');
    Route::get('ingredients/{ingredient}', 'IngredientController@show');
    Route::put('ingredients/{ingredient}', 'IngredientController@update');
    Route::delete('ingredients/{ingredient}', 'IngredientController@destroy');
    // Visit
    Route::get('visits', 'VisitController@index');
    Route::post('visits', 'VisitController@store');
    Route::get('visits/{visit}', 'VisitController@show');
    Route::put('visits/{visit}', 'VisitController@update');
    Route::delete('visits/{visit}', 'VisitController@destroy');
    // Tax
    Route::get('taxes', 'TaxController@index');
    Route::post('taxes', 'TaxController@store');
    Route::get('taxes/{tax}', 'TaxController@show');
    Route::put('taxes/{tax}', 'TaxController@update');
    Route::delete('taxes/{tax}', 'TaxController@destroy');
});
