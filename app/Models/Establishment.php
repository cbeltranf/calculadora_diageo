<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Establishment extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "name", 'stratum', 'longitude', 'latitude', 'segment_id', 'city_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @param array $attributes
     * @return array $array
     */
    public function setDataCreate($attributes)
    {
        $array["name"] = $attributes["name"];
        $array["stratum"] = array_key_exists('stratum', $attributes) ? $attributes["stratum"] : null;
        $array["longitude"] = $attributes["longitude"];
        $array["latitude"] = $attributes["latitude"];
        $array["segment_id"] = $attributes["segment_id"];
        $array["city_id"] = $attributes["city_id"];
        return $array;
    }

    /**
     * @param array $attributes
     * @return $this
     */
    public function setDataUpdate($attributes)
    {
        $this->name = $attributes["name"];
        $this->stratum = array_key_exists('stratum', $attributes) ? $attributes["stratum"] : $this->statum;
        $this->longitude = $attributes["longitude"];
        $this->latitude = $attributes["latitude"];
        $this->segment_id = $attributes["segment_id"];
        $this->city_id = $attributes['city_id'];
        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function segment()
    {
        return $this->belongsTo(Segment::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
