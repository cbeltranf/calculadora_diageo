<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ingredient extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "name", 'quantity', 'status', 'price_unit', 'measurement_unit_id', 'product_id', 'country_id', 'product_diageo',
        'purchase_price', 'purchase_quantity',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @param array $attributes
     * @return array $array
     */
    public function setDataCreate($attributes)
    {
        $array["name"] = $attributes["name"];
        $array["quantity"] = $attributes["quantity"];
        $array["measurement_unit_id"] = $attributes["measurement_unit_id"];
        $array["product_id"] = $attributes["product_id"];
        $array["product_diageo"] = $attributes["product_diageo"];
        $array["purchase_price"] = $attributes["purchase_price"];
        $array["purchase_quantity"] = $attributes["purchase_quantity"];
        $array["price_unit"] = array_key_exists('price_unit', $attributes) ? $attributes["price_unit"] : null;
        $array["country_id"] = auth()->user()->country_id;
        return $array;
    }

    /**
     * @param array $attributes
     * @return $this
     */
    public function setDataUpdate($attributes)
    {
        $this->name = $attributes["name"];
        $this->quantity = $attributes["quantity"];
        $this->measurement_unit_id = $attributes["measurement_unit_id"];
        $this->product_id = $attributes["product_id"];
        $this->product_diageo = $attributes["product_diageo"];
        $this->purchase_price = $attributes["purchase_price"];
        $this->purchase_quantity = $attributes["purchase_quantity"];
        $this->status = $attributes["status"];        
        if (array_key_exists('price_unit', $attributes) && $attributes["price_unit"]) {
            $this->price_unit = $attributes["price_unit"];
        }
        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function measurementUnit()
    {
        return $this->belongsTo(MeasurementUnit::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
