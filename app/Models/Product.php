<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Product extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "name", 'image', 'sku', 'capacity', 'quantity_shorts', 'cocktail_sales_price', 'description', 'status', 'brand_id',
        'measurement_unit_id', 'service_measurement_id', 'user_id', 'country_id', 'banner'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @param array $attributes
     * @return array $array
     */
    public function setDataCreate($attributes)
    {
        $array["name"] = $attributes["name"];
        $array["image"] = $this->saveFile($attributes);
        $array["banner"] = $this->saveFile($attributes);
        $array["capacity"] = $attributes["capacity"];
        $array["quantity_shorts"] = $attributes["quantity_shorts"];
        $array["cocktail_sales_price"] = $attributes["cocktail_sales_price"];
        $array["description"] = array_key_exists('description', $attributes) ? $attributes["description"] : null;
        $array["brand_id"] = $attributes["brand_id"];
        $array["measurement_unit_id"] = MeasurementUnit::where('abbreviation', 'ML')->value('id');
        $array["service_measurement_id"] = $attributes["service_measurement_id"];
        $array["user_id"] = auth()->user()->id;
        $array["country_id"] = auth()->user()->country_id;
        return $array;
    }

    /**
     * @param array $attributes
     * @return $this
     */
    public function setDataUpdate($attributes)
    {
        $this->name = $attributes["name"];
        if (array_key_exists('image', $attributes) && $this->image !== $attributes["image"]) {
            $this->deleteFile($this->image, 'image');
            $this->image = $this->saveFile($attributes);
        }
        if (array_key_exists('banner', $attributes) && $this->image !== $attributes["banner"]) {
            $this->deleteFile($this->banner);
            $this->banner = $this->saveFile($attributes, 'banner');
        }
        $this->capacity = $attributes["capacity"];
        $this->quantity_shorts = $attributes["quantity_shorts"];
        $this->service_measurement_id = $attributes["service_measurement_id"];
        $this->cocktail_sales_price = $attributes["cocktail_sales_price"];
        $this->description = $attributes["description"];
        $this->status = $attributes["status"];
        $this->brand_id = $attributes["brand_id"];
        return $this;
    }

    /**
     * @param array $attributes
     * @return string $filePath
     */
    private function saveFile($attributes, $key)
    {
        $disk = Storage::disk('public');
        $file = $attributes[$key];
        $extension = $file->getClientOriginalExtension();
        $name = Str::random(50) . '.' . $extension;
        $filePath = 'products/' . $name;
        $disk->put($filePath, file_get_contents($file), 'public');
        return $filePath;
    }

    /**
     * @param string $file
     */
    private function deleteFile($file)
    {
        $disk = Storage::disk('public');
        $disk->exists($file);
        $disk->delete($file);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function measurementUnit()
    {
        return $this->belongsTo(MeasurementUnit::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function serviceMeasurements()
    {
        return $this->belongsTo(ServiceMeasurement::class);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ingredients()
    {
        return $this->hasMany(Ingredient::class);
    }
}
