<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VisitsTax extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "value", 'visit_id', 'tax_id',
    ];

    public function setDataCreate(Visit $visit)
    {
        $taxes = Tax::where('country_id', $visit->country_id)
            ->get();
        foreach ($taxes as $tax) {
            $taxNew = new VisitsTax([
                'value' => $tax->value,
                'visit_id' => $visit->id,
                'tax_id' => $tax->id,
            ]);
            $taxNew->save();
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function visit()
    {
        return $this->belongsTo(Visit::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tax()
    {
        return $this->belongsTo(Tax::class);
    }
}
