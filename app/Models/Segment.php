<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Segment extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "name", 'status', 'country_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @param array $attributes
     * @return array $array
     */
    public function setDataCreate($attributes)
    {
        $array["name"] = $attributes["name"];
        $array["country_id"] = auth()->user()->country_id;
        return $array;
    }

    /**
     * @param array $attributes
     * @return $this
     */
    public function setDataUpdate($attributes)
    {
        $this->name = $attributes["name"];
        $this->status = $attributes["status"];
        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function establishments()
    {
        return $this->hasMany(Establishment::class);
    }
}
