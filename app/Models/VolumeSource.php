<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class VolumeSource extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "name", 'description', 'status', 'country_id', 'volume_source_measurement_id','observations'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @param array $attributes
     * @return array $array
     */
    public function setDataCreate($attributes)
    {
        $array["name"] = $attributes["name"];
        $array["description"] = array_key_exists('description', $attributes) ? $attributes["description"] : null;
        $array["image"] = $this->saveFile($attributes);
        $array["country_id"] = auth()->user()->country_id;
        $array["volume_source_measurement_id"] =  (!empty($attributes["volume_source_measurement_id"])) ? $attributes["volume_source_measurement_id"] : null;
        $array["observations"] = $attributes["observations"];
        return $array;
    }

    /**
     * @param array $attributes
     * @return $this
     */
    public function setDataUpdate($attributes)
    {
        $this->name = $attributes["name"];
        if (array_key_exists('image', $attributes) && $this->image !== $attributes["image"]) {
            $this->deleteFile($this->image);
            $this->image = $this->saveFile($attributes);
        }
        $this->description = array_key_exists('description', $attributes) ? $attributes["description"] : $this->description;
        $this->status = $attributes["status"];
        $this->volume_source_measurement_id = (!empty($attributes["volume_source_measurement_id"]))? $attributes["volume_source_measurement_id"] : null;;
        $this->observations = $attributes["observations"];
        return $this;
    }

    /**
     * @param array $attributes
     * @return string $filePath
     */
    private function saveFile($attributes)
    {
        $disk = Storage::disk('public');
        $file = $attributes["image"];
        $extension = $file->getClientOriginalExtension();
        $name = Str::random(50) . '.' . $extension;
        $filePath = 'volumeSources/' . $name;
        $disk->put($filePath, file_get_contents($file), 'public');
        return $filePath;
    }

    /**
     * @param string $file
     */
    private function deleteFile($file)
    {
        $disk = Storage::disk('public');
        $disk->exists($file);
        $disk->delete($file);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function volumeSourceMeasurement()
    {
        return $this->belongsTo(VolumeSourceMeasurement::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function brands()
    {
        return $this->hasMany(Brand::class);
    }
}
