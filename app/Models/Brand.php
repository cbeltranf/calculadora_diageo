<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "name", 'status', 'volume_source_id', 'image',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @param array $attributes
     * @return array $array
     */
    public function setDataCreate($attributes)
    {
        $array["name"] = $attributes["name"];
        $array["volume_source_id"] = $attributes["volume_source_id"];
        $array["image"] = $attributes["image"];
        return $array;
    }

    /**
     * @param array $attributes
     * @return $this
     */
    public function setDataUpdate($attributes)
    {
        $this->name = $attributes["name"];
        $this->image = $attributes["image"];
        $this->status = $attributes["status"];
        $this->volume_source_id = $attributes['volume_source_id'];
        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function volumeSource()
    {
        return $this->belongsTo(VolumeSource::class);
    }
}
