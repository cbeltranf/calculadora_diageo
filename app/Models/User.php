<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "name", "last_name", "email", "password", "document", "login_alt", "social_token", "can_notify",
        "updated_data", "phone", "country_phone", "birthday", "session_type", "avatar", "status", "language",
        "first_login", "last_login", "platform", "model", "version", "uuid", "date_password_change", "recovery_code",
        "country_id", "city_id", "profile_id",
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @param array $attributes
     * @return array $array
     */
    public function setDataCreate($attributes)
    {
        $array["name"] = $attributes["name"];
        $array["last_name"] = $attributes["last_name"];
        $array["email"] = $attributes["email"];
        $array["password"] = bcrypt($attributes["password"]);
        $array["phone"] = array_key_exists('phone', $attributes) ? $attributes["phone"] : null;
        $array["birthday"] = array_key_exists('birthday', $attributes) ? $attributes["birthday"] : null;
        $array["platform"] = array_key_exists('platform', $attributes) ? $attributes["platform"] : null;
        $array["model"] = array_key_exists('model', $attributes) ? $attributes["model"] : null;
        $array["version"] = array_key_exists('version', $attributes) ? $attributes["version"] : null;
        $array["uuid"] = array_key_exists('uuid', $attributes) ? $attributes["uuid"] : null;
        $array["token"] = array_key_exists('token', $attributes) ? $attributes["token"] : null;
        $array["session_type"] = array_key_exists('session_type', $attributes) ? $attributes["session_type"] : "M";
        $array["can_notify"] = array_key_exists('can_notify', $attributes) ? $attributes["can_notify"] : "N";
        $array["country_id"] = $attributes["country_id"];
        $array["profile_id"] = $attributes["profile_id"];
        $array["city_id"] = $attributes["city_id"];
        $array["branch_id"] = array_key_exists('branch_id', $attributes) ? $attributes["branch_id"] : null;
        return $array;
    }

    /**
     * @param array $attributes
     * @return $this
     */
    public function setDataUpdate($attributes)
    {
        $this->name = $attributes["name"];
        $this->email = $attributes["email"];
        if (array_key_exists('password', $attributes["password"])) {
            $this->password = bcrypt($attributes["password"]);
        }
        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
