<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Visit extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "cost_volume_source", 'price_volume_source', 'cost_product', 'price_product',
        'type_result',
        'capacity_product', 'ml_shot_product', 'ml_shot_volume_source', 'volume_source_sales_price_shot', 'capacity_volume_source',
        'quantity_shorts_product', 'cocktail_sales_price_product', 'profitability_shots', 'profitability_bottle',
        'profit_shots_percentage', 'profit_shots_diference', 'profit_bottle_percentage', 'profit_bottle_diference',
        'establishment_id', 'volume_source_id', 'product_id', 'user_id', 'country_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @param array $attributes
     * @param Establishment $establishment
     * @return array $array
     */
    public function setDataCreate($attributes, $establishment)
    {
        $product = Product::findOrFail($attributes["product_id"]);
        $array["cost_volume_source"] = $attributes["cost_volume_source"];
        $array["type_result"] = $attributes["type_result"];
        $array["price_volume_source"] = $attributes["price_volume_source"];
        $array["capacity_volume_source"] = $product->capacity;
        $array["ml_shot_volume_source"] = $product->ml_shot_product;
        $array["volume_source_sales_price_shot"] = $attributes["vsPriceShot"]; //($attributes["price_volume_source"] / $product->capacity ) * $product->ml_shot_product;
        $array["cost_product"] = $attributes["cost_product"];
        $array["price_product"] = $attributes["price_product"];
        $array["capacity_product"] = $product->capacity;
        $array["ml_shot_product"] = $product->ml_shot_product;
        $array["quantity_shorts_product"] = $product->quantity_shorts;
        $array["cocktail_sales_price_product"] = $attributes["priceShot"]; //  //$product->cocktail_sales_price;
        $array["establishment_id"] = $establishment->id;
        $array["volume_source_id"] = $attributes["volume_source_id"];
        $array["product_id"] = $attributes["product_id"];
        $array["user_id"] = auth()->user()->id;
        $array["country_id"] = auth()->user()->country_id;
        return $array;
    }

    /**
     * @param array $attributes
     * @return $this
     */
    public function setDataUpdate($attributes)
    {
        $this->cost_volume_source = $attributes["cost_volume_source"];
        $this->price_volume_source = $attributes["price_volume_source"];
        $this->type_result = $attributes["type_result"];
        $this->capacity_volume_source = $attributes["capacity_volume_source"];
        $this->ml_shot_volume_source =   $attributes["ml_shot_volume_source"];
        $this->volume_source_sales_price_shot =   $attributes["volume_source_sales_price_shot"];
        if(!empty($attributes["cocktail_sales_price_product"])){ $this->cocktail_sales_price_product =   $attributes["cocktail_sales_price_product"]; }
        if(!empty($attributes["price_product"])){ $this->price_product = $attributes["price_product"]; }
        $this->cost_product = $attributes["cost_product"];
        $this->ml_shot_product = $attributes["ml_shot_product"];
        $this->capacity_product = $attributes["capacity_product"];;
        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function establishment()
    {
        return $this->belongsTo(Establishment::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function volumeSource()
    {
        return $this->belongsTo(VolumeSource::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function visitIngredients()
    {
        return $this->hasMany(VisitIngredient::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function taxes()
    {
        return $this->hasMany(VisitsTax::class);
    }
}
