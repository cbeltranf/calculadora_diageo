<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VisitIngredient extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'quantity', 'price_unit', 'ingredient_id', 'visit_id', 'product_diageo', 'purchase_price', 'purchase_quantity'
    ];

    /**
     * @param Visit $visit
     */
    public function setDataCreate(Visit $visit)
    {
        $product = $visit->product()->first();
        $ingredients = $product->ingredients()->get();
        foreach ($ingredients as $ingredient) {
            $visitIngredient = new VisitIngredient([
                'quantity' => $ingredient->quantity,
                'price_unit' => $ingredient->price_unit,
                'purchase_price' => $ingredient->purchase_price,
                'purchase_quantity' => $ingredient->purchase_quantity,                  
                'ingredient_id' => $ingredient->id,
                'visit_id' => $visit->id,
                'product_diageo' => $ingredient->product_diageo,
            ]);
            $visitIngredient->save();
        }
    }

    /**
     * @param array $attributes
     * @return $this
     */
    public function setDataUpdate($attributes)
    {
        $this->quantity = $attributes["quantity"];
        $this->price_unit = $attributes["price_unit"];
        $this->purchase_price = $attributes["purchase_price"];
        $this->purchase_quantity = $attributes["purchase_quantity"];
        $this->product_diageo = $attributes["product_diageo"];
        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ingredient()
    {
        return $this->belongsTo(Ingredient::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function visit()
    {
        return $this->belongsTo(Visit::class);
    }
}
