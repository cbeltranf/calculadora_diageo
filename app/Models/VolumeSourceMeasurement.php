<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class VolumeSourceMeasurement extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "name"
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @param array $attributes
     * @return array $array
     */
    public function setDataCreate($attributes)
    {
        $array["name"] = $attributes["name"];
        return $array;
    }

    /**
     * @param array $attributes
     * @return $this
     */
    public function setDataUpdate($attributes)
    {
        $this->name = $attributes["name"];
        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function volumeSources()
    {
        return $this->hasMany(VolumeSource::class);
    }
}
