<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class AppInfo extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type', 'extra', 'link_update'
    ];

    public function setDataCreate(Request $request)
    {
        $array["type"] = $request->type;
        $array["extra"] = $request->extra;
        $array["link_update"] = $request->link_update;
        return $array;
    }

    public function setDataUpdate(Request $request)
    {
        $this->type = $request->type;
        $this->extra = $request->extra;
        $this->link_update = $request->link_update;
        return $this;
    }
}
