<?php

namespace App\Http\Requests\VolumeSource;

use Illuminate\Foundation\Http\FormRequest;

class VolumeSourceCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:1|max:191',
            'description' => 'nullable',
            'image'=> 'required',
        ];
    }
}
