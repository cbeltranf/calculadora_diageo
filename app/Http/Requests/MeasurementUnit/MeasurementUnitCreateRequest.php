<?php

namespace App\Http\Requests\MeasurementUnit;

use Illuminate\Foundation\Http\FormRequest;

class MeasurementUnitCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:1|max:191',
            'abbreviation' => 'required|string|min:1|max:191',
        ];
    }
}
