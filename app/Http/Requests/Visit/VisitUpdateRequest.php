<?php

namespace App\Http\Requests\Visit;

use Illuminate\Foundation\Http\FormRequest;

class VisitUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cost_volume_source' => 'nullable|integer',
            'type_result' => 'required',
            'price_volume_source' => 'nullable|integer',
            'cost_product' => 'nullable|integer',
            'price_product' => 'nullable|integer',
        ];
    }
}
