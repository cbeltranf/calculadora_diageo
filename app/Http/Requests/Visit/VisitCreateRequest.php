<?php

namespace App\Http\Requests\Visit;

use Illuminate\Foundation\Http\FormRequest;

class VisitCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cost_volume_source' => 'nullable|integer',
            'price_volume_source' => 'nullable|integer',
            'type_result' => 'required',
            'cost_product' => 'nullable|integer',
            'price_product' => 'nullable|integer',
            'volume_source_id' => 'required|integer|exists:volume_sources,id',
            'product_id' => 'required|integer|exists:products,id',
            'establishment' => 'required',
        ];
    }
}
