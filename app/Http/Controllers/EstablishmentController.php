<?php

namespace App\Http\Controllers;

use App\Http\Requests\Establishment\EstablishmentCreateRequest;
use App\Http\Requests\Establishment\EstablishmentUpdateRequest;
use App\Models\City;
use App\Models\Establishment;
use App\Models\State;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class EstablishmentController extends Controller
{
    use ApiResponse;

    /**
     * @var Establishment
     */
    protected $establishment;

    /**
     * EstablishmentController constructor.
     * @param Establishment $establishment
     */
    public function __construct(Establishment $establishment)
    {
        $this->middleware([
            'auth:api'
        ]);
        $this->establishment = $establishment;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $statesId = State::where('country_id', auth()->user()->country_id)
            ->pluck('id')
            ->toArray();
        $citiesId = City::whereIn('state_id', $statesId)->pluck('id')->toArray();
        $establishments = Establishment::whereIn('city_id', $citiesId)
            ->get();
        $establishments = $this->showAllApp($establishments);
        return $this->successResponse($establishments, Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param EstablishmentCreateRequest $establishmentCreateRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(EstablishmentCreateRequest $establishmentCreateRequest)
    {
        $attributes = $establishmentCreateRequest->all();
        $dataCreate = $this->establishment->setDataCreate($attributes);
        $this->establishment = $this->establishment->create($dataCreate);
        $this->establishment = $this->showOneApp($this->establishment);
        return $this->successResponse($this->establishment, Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param Establishment $establishment
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Establishment $establishment)
    {
        $establishment = $this->showOneApp($establishment);
        return $this->successResponse($establishment, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EstablishmentUpdateRequest $establishmentUpdateRequest
     * @param Establishment $establishment
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(EstablishmentUpdateRequest $establishmentUpdateRequest, Establishment $establishment)
    {
        $attributes = $establishmentUpdateRequest->all();
        $establishment = $establishment->setDataUpdate($attributes);
        if ($establishment->isClean()) {
            return $this->responseIsClean($establishmentUpdateRequest);
        }
        $establishment->save();
        $establishment = $this->showOneApp($establishment);
        return $this->successResponse($establishment, Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     * @param Establishment $establishment
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Establishment $establishment)
    {
        $establishment->delete();
        $establishment = $this->showOneApp($establishment);
        return $this->successResponse($establishment, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request)
    {
        $request->validate([
            'city' => 'required|integer|exists:cities,id',
            'name' => 'required|string|min:1|max:191',
        ]);
        $cityId = $request->city;
        $name = $request->name;
        $establishments = Establishment::where('city_id', $cityId)
            ->where('name', 'like', "%$name%")
            ->get();
        $establishments = $this->showAllApp($establishments);
        return $this->successResponse($establishments, Response::HTTP_OK);
    }

    private function showAllApp($establishments)
    {
        $data = [];
        foreach ($establishments as $establishment) {
            $segment = $establishment->segment()->first();
            $city = $establishment->city()->first();
            array_push($data, [
                'id' => $establishment->id,
                'name' => $establishment->name,
                'stratum' => $establishment->stratum,
                'longitude' => $establishment->longitude,
                'latitude' => $establishment->latitude,
                'segment_id' => $establishment->segment_id,
                'city_id' => $establishment->city_id,
                'segment' => [
                    'id' => $segment->id,
                    'name' => $segment->name,
                ],
                'city' => [
                    'id' => $city->id,
                    'name' => $city->name,
                ],
            ]);
        }
        return $data;
    }

    private function showOneApp(Establishment $establishment)
    {
        $segment = $establishment->segment()->first();
        $city = $establishment->city()->first();
        return [
            'id' => $establishment->id,
            'name' => $establishment->name,
            'stratum' => $establishment->stratum,
            'longitude' => $establishment->longitude,
            'latitude' => $establishment->latitude,
            'segment_id' => $establishment->segment_id,
            'city_id' => $establishment->city_id,
            'segment' => [
                'id' => $segment->id,
                'name' => $segment->name,
            ],
            'city' => [
                'id' => $city->id,
                'name' => $city->name,
            ],
        ];
    }
}
