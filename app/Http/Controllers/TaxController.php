<?php

namespace App\Http\Controllers;

use App\Http\Requests\Tax\TaxCreateRequest;
use App\Http\Requests\Tax\TaxUpdateRequest;
use App\Models\Tax;
use App\Traits\ApiResponse;
use Illuminate\Http\Response;

class TaxController extends Controller
{
    use ApiResponse;

    /**
     * @var Tax
     */
    protected $tax;

    /**
     * TaxController constructor.
     * @param Tax $tax
     */
    public function __construct(Tax $tax)
    {
        $this->middleware([
            'auth:api'
        ]);
        $this->tax = $tax;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $taxes = Tax::where('country_id', auth()->user()->country_id)
            ->latest('id')
            ->get();
        return $this->successResponse($taxes, Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TaxCreateRequest $taxCreateRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(TaxCreateRequest $taxCreateRequest)
    {
        $attributes = $taxCreateRequest->all();
        $dataCreate = $this->tax->setDataCreate($attributes);
        $this->tax = $this->tax->create($dataCreate);
        return $this->successResponse($this->tax, Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param Tax $tax
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Tax $tax)
    {
        return $this->successResponse($tax, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param TaxUpdateRequest $taxUpdateRequest
     * @param Tax $tax
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(TaxUpdateRequest $taxUpdateRequest, Tax $tax)
    {
        $attributes = $taxUpdateRequest->all();
        $tax = $tax->setDataUpdate($attributes);
        if ($tax->isClean()) {
            return $this->responseIsClean($taxUpdateRequest);
        }
        $tax->save();
        return $this->successResponse($tax, Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     * @param Tax $tax
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Tax $tax)
    {
        $tax->delete();
        return $this->successResponse($tax, Response::HTTP_OK);
    }
}
