<?php

namespace App\Http\Controllers;

use App\Http\Requests\Visit\VisitCreateRequest;
use App\Http\Requests\Visit\VisitUpdateRequest;
use App\Models\Establishment;
use App\Models\Ingredient;
use App\Models\Product;
use App\Models\Visit;
use App\Models\VisitIngredient;
use App\Models\ServiceMeasurement;
use App\Models\VisitsTax;
use App\Traits\ApiResponse;
use Illuminate\Http\Response;

class VisitController extends Controller
{
    use ApiResponse;

    /**
     * @var Visit
     */
    protected $visit;

    /**
     * VisitController constructor.
     * @param Visit $visit
     */
    public function __construct(Visit $visit)
    {
        $this->middleware([
            'auth:api'
        ]);
        $this->visit = $visit;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $visits = $this->visit->where('country_id', auth()
        ->user()->country_id)
        ->get();
        return $this->successResponse($visits, Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param VisitCreateRequest $visitCreateRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(VisitCreateRequest $visitCreateRequest)
    {
        $attributes = $visitCreateRequest->all();
        $establishment = Establishment::find($attributes["establishment"]["id"]);
        if (!$establishment) {
            $establishment = new Establishment();
            $dataCreate = $establishment->setDataCreate($attributes["establishment"]);
            $establishment = $establishment->create($dataCreate);
        }
        $dataCreate = $this->visit->setDataCreate($attributes, $establishment);
        $this->visit = $this->visit->create($dataCreate);
        $visitsTax = new VisitsTax();
        $visitsTax->setDataCreate($this->visit);
        $visitIngredients = new VisitIngredient();
        $visitIngredients->setDataCreate($this->visit);
        $this->visit = $this->showOneApp($this->visit);
        return $this->successResponse($this->visit, Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param Visit $visit
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Visit $visit)
    {
        $visit = $this->showOneApp($visit);
        return $this->successResponse($visit, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param VisitUpdateRequest $visitUpdateRequest
     * @param Visit $visit
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(VisitUpdateRequest $visitUpdateRequest, Visit $visit)
    {
        $attributes = $visitUpdateRequest->all();

        /*if ($visit->isClean()) {
            return $this->responseIsClean($visitUpdateRequest);
        }*/


        $product = $visit->product()->first();
        $productServiceMeasurement =  ServiceMeasurement::find($product->service_measurement_id);

        if($visit->type_result == 'Shot'){
            $attributes["volume_source_sales_price_shot"] = $attributes["price_volume_source"];
            $attributes["price_volume_source"] = '';
        }

        if($productServiceMeasurement->id == 2){
            //bottle
            $attributes["price_product"] = $attributes["cocktail_sales_price_product"];
            unset($attributes["cocktail_sales_price_product"]);
        }else{
            $attributes["cocktail_sales_price_product"] = $attributes["cocktail_sales_price_product"];
            unset($attributes["price_product"]);
        }



        foreach($attributes["ingredients"] as $ing){
            $ingredient = VisitIngredient::find($ing["id"]);
            $ingredient->quantity = $ing["quantity"];
            $ingredient->purchase_price = $ing["purchase_price"];
            $ingredient->purchase_quantity = $ing["purchase_quantity"];
            if($ingredient->product_diageo == 'N'){
                 $ingredient->price_unit = $ingredient->purchase_price / $ingredient->purchase_quantity;
            }else{
                $attributes["capacity_product"] = $ing["purchase_quantity"];
            }
            //echo '<br>';
            $ingredient->save();
            ///dd($ingredient);
        }
        $visit = $visit->setDataUpdate($attributes);
        $visit->save();

        //$visit->save();
        $visit = $this->showOneApp($visit);
        return $this->successResponse($visit, Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     * @param Visit $visit
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Visit $visit)
    {
        $visit->delete();
        return $this->successResponse($visit, Response::HTTP_OK);
    }

    /**
     * @param Visit $visit
     * @return array
     */
    public function showOneApp(Visit $visit)
    {
        $product = $visit->product()->first();
        $productServiceMeasurement =  ServiceMeasurement::find($product->service_measurement_id);
        $volumeSource = $visit->volumeSource()->first();
        $volumeSourceMeasurement = $volumeSource->volumeSourceMeasurement()->first();
        $establishment = $visit->establishment()->first();
        $brand = $product->brand()->first();
        $measurementUnit = $product->measurementUnit()->first();
        $ingredients = $this->getIngredients($visit);
        $brand =  $product->brand()->first();
        $taxes = $this->getTaxes($visit, $visit->price_product);
        //$costEffectiveness = $this->getCostEffectiveness($visit, $product, $volumeSource, $total_taxes);
        $calc_product_diageo = $this->getCalcProductDiageo($visit, $product, $ingredients, $taxes);

        $taxes = $this->getTaxes($visit, $visit->price_volume_source);
        $calc_volume_source = $this->getCalcVolumeSource($visit, $product, $ingredients, $taxes);

        return [
            'id' => $visit->id,
            'type_result' => $visit->type_result,
            'cost_volume_source' => $visit->cost_volume_source,
            'price_volume_source' => $visit->price_volume_source,
            'cost_product' => $visit->cost_product,
            'price_product' => $visit->price_product,
            "capacity_volume_source" => $visit->capacity_volume_source,
            "ml_shot_volume_source" => $visit->ml_shot_volume_source,
            "capacity_product" => $visit->capacity_product,
            "ml_shot_product" => $visit->ml_shot_product,
            "cocktail_sales_price_product" =>  $visit->cocktail_sales_price_product,
            "volume_source_sales_price_shot" => $visit ->volume_source_sales_price_shot,
            'service_measurement'=> $productServiceMeasurement,
            'volume_source' => [
                'id' => $volumeSource->id,
                'name' => $volumeSource->name,
                'image' => $volumeSource->image,
                'observations' => $volumeSource->observations,
                'volume_source_measurement_id'=> $volumeSource->volume_source_measurement_id,
                'volume_source_measurement' => [
                    'id' => !empty($volumeSourceMeasurement->id) ? $volumeSourceMeasurement->id : '',
                    'name' => !empty($volumeSourceMeasurement->name) ? $volumeSourceMeasurement->name : '',
                ],
            ],
            'profability' => [
                'product_diageo' => $calc_product_diageo,
                'volume_source' => $calc_volume_source,
            ],
            'product' => [
                'id' => $product->id,
                'brand_image'=> $brand->image,
                'name' => $product->name,
                'image' => $product->image,
                'banner' => $product->banner,
                'capacity' => $product->capacity,
                'quantity_shorts' => $product->quantity_shorts,
                'cocktail_sales_price' => $product->cocktail_sales_price,
                'description' => $product->description,
                'brand' => [
                    'id' => $brand->id,
                    'name' => $brand->name,
                ],
                'measurement_unit' => [
                    'id' => $measurementUnit->id,
                    'name' => $measurementUnit->name,
                    'abbreviation' => $measurementUnit->abbreviation,
                ],
                'video' => $product->video,
                'preparation' => $product->preparation,
                'ingredients' => $ingredients,
            ],
            'establishment' => [
                'id' => $establishment->id,
                'name' => $establishment->name,
                'stratum' => $establishment->stratum,
            ],
            'taxes' => $taxes,
        ];
    }



    /**
     * @param Visit $visit
     * @return array
     */
    private function getIngredients(Visit $visit)
    {
        $visitIngredients = $visit->visitIngredients()->get();
        $data = [];
        foreach ($visitIngredients as $visitIngredient) {
            $ingredient = $visitIngredient->ingredient()->first();
            $measurementUnit = $ingredient->measurementUnit->first();
            array_push($data, [
                'id' => $visitIngredient->id,
                'name' => $ingredient->name,
                'product_diageo' => $ingredient->product_diageo,
                'quantity' => $visitIngredient->quantity,
                'price_unit' => $visitIngredient->price_unit,
                'price_total' => $visitIngredient->price_unit ? $visitIngredient->price_unit * $visitIngredient->quantity : null,

                'purchase_price' => $visitIngredient->purchase_price,
                'purchase_quantity' => $visitIngredient->purchase_quantity,

                'measurement_unit' => [
                    'id' => $measurementUnit->id,
                    'name' => $measurementUnit->name,
                    'abbreviation' => $measurementUnit->abbreviation,
                ],
            ]);

        }
        return $data;
    }

    /**
     * @param Visit $visit
     * @param $price_volume
     * @return array
     */
    private function getTaxes(Visit $visit, $price_volume)
    {
        $visitsTaxes = $visit->taxes()->get();
        $data = [];
        foreach ($visitsTaxes as $visitsTax) {
            $tax = $visitsTax->tax()->first();
            array_push($data, [
                'id' => $visitsTax->id,
                'value' => $visitsTax->value,
                'price' => ($price_volume * $visitsTax->value) / 100,
                'tax' => [
                    'id' => $tax->id,
                    'name' => $tax->name,
                ],
            ]);
        }
        return $data;
    }


    /**
     * @param $taxes
     * @return double
     */
    private function getSumTaxes($taxes)
    {
        $sum = 0;
        foreach ($taxes as $k => $v) {
            $sum += $v["price"];
        }

        return $sum;
    }


    /**
     * @param $visit
     * @param $product
     * @param $ingredients
     * @param $taxes
     * @param $cant_coctails
     * @return array
     */
    private function getCalcVolumeSource($visit, $product, $ingredients, $taxes)
    {

        $cant_coctails = $visit->capacity_product / $visit->ml_shot_product;

        $volumeSource = $visit->volumeSource()->first();
        $volumeSourceMeasurement = $volumeSource->volumeSourceMeasurement()->first();

        $cost_ml_volume = $visit->cost_volume_source /  $visit->capacity_volume_source;

        $cost_x_shot = $visit->cost_volume_source /  $visit->ml_shot_product;
        // Se iguala la capacidad de la fuente de volumen a la del producto diageo
        $total_volume = $cost_ml_volume * $visit->capacity_volume_source;
        $other_expenses = ($total_volume * 5) / 100;
        $total_taxes = $this->getSumTaxes($taxes);
        $total_volume_cost = $total_volume + $other_expenses + $total_taxes;
        //$product_cost_percentage = ($total_volume_cost / $visit->price_volume_source) * 100;

        if($visit->type_result != 'Shot'){
            $profitability_after_taxes = $visit->price_volume_source - $total_volume_cost;
            $profitability_percentage = ($profitability_after_taxes / $visit->price_volume_source) * 100;
        }else{
            $profitability_after_taxes = 0;
            $profitability_percentage = 0;
        }


        $profitability_shots = '';
        $profitability_bottle = '';

        $cant_shots_volume = $visit->capacity_volume_source /  $visit->ml_shot_volume_source;

        $cant_source = 1;


        if ($volumeSource->id == 3) { // Cerveza importada (Comparar por enum)
            $profitability_after_taxes = $profitability_after_taxes; //* $cant_coctails;
            $profitability_percentage = (!empty($visit->price_volume_source))?($profitability_after_taxes / ($visit->price_volume_source * $cant_coctails)) * 100:0;
            $cant_source = $cant_coctails;

            $profitability_shots =  $profitability_after_taxes;
            $profitability_bottle = $profitability_after_taxes;

        } else if (!empty($volumeSource->volume_source_measurement_id)) {
            //if($volumeSource->volume_source_measurement_id == 1){ // Botella  ENUM (?)
            $profitability_shots = ($profitability_after_taxes / $cant_shots_volume);
            $profitability_bottle = $profitability_after_taxes;
            /* }else{
                  $profitability_shots = $profitability_after_taxes;
                  $profitability_bottle = $profitability_after_taxes*$product->capacity;
             }*/
        }

        //////////


        $other_expenses = ($visit->cost_volume_source * 5) / 100;

        $sales_taxes_detail_fv = $this->getTaxes($visit, $visit->price_volume_source);
        $sales_taxes_fv = $this->getSumTaxes($sales_taxes_detail_fv);


        $f_cost = ($visit->cost_volume_source + $other_expenses);
        $f_rent = ($visit->price_volume_source - $f_cost  - $sales_taxes_fv);

        $costo_por_shot = (($visit->cost_volume_source ) / $cant_shots_volume);
        $sales_taxes_shot_fv_detail = $this->getTaxes($visit, $visit->volume_source_sales_price_shot);
        $sales_taxes_shot_fv = $this->getSumTaxes($sales_taxes_shot_fv_detail);
        $f_rent_shot = $visit->volume_source_sales_price_shot - $sales_taxes_shot_fv - $costo_por_shot;

        if($visit->type_result != 'Shot'){
            $total_ingredients = $visit->cost_volume_source;
            $sales_price = $visit->price_volume_source;
            $cost_percent = ($visit->price_volume_source > 0)?($f_cost / $visit->price_volume_source) * 100:0;
            $rent_percent = ($visit->price_volume_source > 0)?($f_rent / $visit->price_volume_source) * 100:0;
        }else{
            $total_ingredients =  $costo_por_shot;
            $other_expenses = $other_expenses / $cant_shots_volume;
            $sales_taxes_detail_fv = $sales_taxes_shot_fv_detail;
            $sales_taxes_fv = $sales_taxes_shot_fv;
            $sales_price =  $visit->volume_source_sales_price_shot;
            $f_cost = ($costo_por_shot + $other_expenses);
            $f_rent = ($sales_price - $f_cost  - $sales_taxes_fv);
            $cost_percent = ($f_cost / $sales_price) * 100 ;
            $rent_percent = ($f_rent / $sales_price) * 100 ;
        }


        return [
            "profability" => $profitability_after_taxes,
            "profability_percentage" => $profitability_percentage,
            "volume_source_units" => $cant_source,
            "cant_comp" => $cant_coctails,
            "taxes"=> $taxes,
            "taxes_sum_price" => $total_taxes,
            "other_expenses" => $other_expenses,
            'profability_detail' => [
                "cost_per_bottle"=>$total_volume_cost,
                "cost_per_shot"=> $costo_por_shot,
                'profitability_shots' => $f_rent_shot,
                'profitability_bottle' => $profitability_bottle,
                'taxes' => $sales_taxes_detail_fv,
                'ml_per_shot' => $visit->ml_shot_volume_source,
                'capacity_volume_source' => $visit->capacity_volume_source,

                "total_ingredients" =>  $total_ingredients, // Total Costo Ing
                "other_expenses" => $other_expenses, // 5% costo
                "total_cost_fv" => $f_cost, //Total Costo
                "fv_sales_price" => $sales_price, // Sales price
                "sales_taxes_fv" => $sales_taxes_fv, // Taxes
                "rentability_fv" =>  $f_rent, // Renta
                "cost_percent" => $cost_percent, // cost %
                "rent_percent" => $rent_percent, // % Rent
            ],
        ];
    }

    /**
     * @param $visit
     * @param $product
     * @param $ingredients
     * @param $taxes
     * @return array
     */
    private function getCalcProductDiageo($visit, $product, $ingredients, $taxes)
    {
        $volumeSource = $visit->volumeSource()->first();
        $volumeSourceMeasurement = $volumeSource->volumeSourceMeasurement()->first();
        $productServiceMeasurement =  ServiceMeasurement::find($product->service_measurement_id);

        $cost_ml = $visit->cost_product / $visit->capacity_product;
        $total_volume = $cost_ml * $visit->capacity_product;
        //$total_taxes = $this->getSumTaxes($taxes);
        $total_volume_cost = $total_volume; //+ $total_taxes
        //$product_cost_percentage = ($total_volume_cost / $visit->price_product) * 100;


        $total_ingredients = 0;
        $cocktail_cost = 0;
        $coctails_per_bottle = 0;
        $cost_ingredients = array();
        $diageo_ingredient_cost = 0;

        foreach ($ingredients as $k => $v) {

            $object = new \stdClass;
            $object->id = $v["id"];
            $object->name = $v["name"];
            $object->price_unit = ($v["product_diageo"]=='Y') ? ($total_volume_cost /  $visit->capacity_product) : $v["price_unit"]; //* $v["quantity"]
            $object->product_diageo = $v["product_diageo"];
            $object->v1 = $total_volume_cost;
            $object->c2 =  $visit->capacity_product;
            $object->v3 = $v["quantity"];
            $object->price_total = $object->price_unit * $v["quantity"];
            $object->quantity = $v["quantity"];
            $object->purchase_price = $v["purchase_price"];
            $object->purchase_quantity = $v["purchase_quantity"];
            $object->measurement_unit = $v["measurement_unit"];
            array_push($cost_ingredients, $object);

            if ($v["product_diageo"]=='Y') {
                $coctails_per_bottle =  $visit->capacity_product / $v["quantity"];
                $diageo_ingredient_cost += $object->price_total;
                $object->purchase_price = $total_volume_cost;
                $object->purchase_quantity = $visit->capacity_product;
            }
                $total_ingredients += $object->price_total;

        }
        //dd($total_ingredients, );
        $other_expenses_bottle = ($total_volume_cost * 5) / 100 ;
        $total_volume_cost_bottle =  $other_expenses_bottle +  $total_volume_cost;

        $other_expenses = (($total_ingredients) * 5) / 100;
        $cocktail_cost += $other_expenses + $total_ingredients; //Other Expenses +$diageo_ingredient_cost

        $cost_coctails_per_bottle = $coctails_per_bottle * $cocktail_cost;
        $sale_price_cocktails = $visit->cocktail_sales_price_product * $coctails_per_bottle;

        // $profitability_after_taxes = $visit->price_product - $total_volume_cost; //- $total_taxes;

        //bottle

        $shots_per_bottle =  $visit->capacity_product / $visit->ml_shot_product;
        $sales_taxes_bottle_detail = $this->getTaxes($visit, $visit->price_product);
        $sales_taxes_bottle = $this->getSumTaxes($sales_taxes_bottle_detail);

        $total_volume_cost_shot =  $total_volume_cost_bottle/$shots_per_bottle;

        $profitability_bottle_after_taxes = 0;
        $profitability_bottle_percentage = 0;
        $profitability_bottle_shots = 0;
        $profitability_bottle_bottle = 0;
        if($visit->type_result != 'Shot'){
            $profitability_bottle_after_taxes = $visit->price_product - $total_volume_cost_bottle - $sales_taxes_bottle;
            $profitability_bottle_percentage = ($profitability_bottle_after_taxes * 100) / $visit->price_product;

            $profitability_bottle_shots = $profitability_bottle_after_taxes / $shots_per_bottle;
            $profitability_bottle_bottle = $profitability_bottle_after_taxes;
        }


        //coctail
        $sales_taxes_detail = $this->getTaxes($visit, $visit->cocktail_sales_price_product);
        $sales_taxes = $this->getSumTaxes($sales_taxes_detail);





        $profitability_after_taxes = $sale_price_cocktails - $cost_coctails_per_bottle - $sales_taxes;
        $profitability_percentage = ($profitability_after_taxes / $cost_coctails_per_bottle) * 100;

        $profitability_shots = $profitability_after_taxes / $coctails_per_bottle;
        $profitability_bottle = $profitability_after_taxes;


        //////////

        if($productServiceMeasurement->id == 2){
            //bottle
            $base_price = $visit->price_product;

        }else{
            $base_price = $visit->cocktail_sales_price_product;
        }
        $sales_taxes_detail_coctail = $this->getTaxes($visit, $base_price);
        $sales_taxes_coctail = $this->getSumTaxes($sales_taxes_detail_coctail);


        $f_cost = ($total_ingredients + $other_expenses);
        $f_rent = ($base_price - $f_cost  - $sales_taxes_coctail);

        return [
            "ingredients" => $cost_ingredients,
            "total_ingredients" => $total_ingredients, // Total Costo Ing
            "profability" => $profitability_after_taxes,
            "profability_percentage" => $profitability_percentage,
            "cant_coctails_per_bottle" => $coctails_per_bottle,
            "cost_coctail_unit" => $cocktail_cost,
            "cost_coctail_per_bottle" => $cost_coctails_per_bottle,
            "sale_price_cocktails" => $sale_price_cocktails,
            'profability_bottle_detail' => [
                'cost_per_shot'=> $total_volume_cost_shot,
                'cost_per_bottle'=>$total_volume_cost_bottle,
                'profitability_shots' => $profitability_bottle_shots,
                'profitability_bottle' => $profitability_bottle_bottle,
                'profit_shots_percentage' => ($profitability_bottle_shots >0)?($profitability_bottle_shots / ($visit->price_product/$shots_per_bottle)) * 100:0,
                'profit_shots_diference' => ($profitability_bottle_shots >0)?($total_volume_cost_bottle/$shots_per_bottle) - $profitability_bottle_shots:0 ,
                'profit_bottle_percentage' => $profitability_bottle_percentage,
                'profit_bottle_diference' =>  $total_volume_cost_bottle - $profitability_bottle_bottle ,
                "other_expenses_bottle" => $other_expenses_bottle,
                'taxes' => $sales_taxes_bottle_detail,
                "sales_taxes_sum_price" => $sales_taxes_bottle,
            ],
            'profability_coctail_detail' => [
                "base" => $base_price,
                "cost_coctail_per_shot" => $cocktail_cost,
                "cost_coctails_per_bottle" => $cost_coctails_per_bottle,
                'profitability_shots' => $profitability_shots,
                'profitability_bottle' => $profitability_bottle,
                'profit_shots_percentage' => ($profitability_shots / $cocktail_cost) * 100,
                'profit_shots_diference' => $profitability_shots - $cocktail_cost,
                'profit_bottle_percentage' => ($profitability_bottle / $cost_coctails_per_bottle) * 100,
                'profit_bottle_diference' => $profitability_bottle - $cost_coctails_per_bottle,
                'taxes' => $sales_taxes_detail_coctail,
                "taxes_sum_price" => $sales_taxes,
                "total_ingredients" => $total_ingredients, // Total Costo Ing
                "other_expenses" => $other_expenses, // 5% costo
                "total_cost_coctail" => $f_cost, //Total Costo
                "coctail_sales_price" => $base_price, // Sales price
                "sales_taxes_coctail" => $sales_taxes_coctail, // Taxes
                "rentability_coctail" =>  $f_rent, // Renta
                "cost_percent" => ($base_price)?($f_cost / $base_price) * 100:0, // cost %
                "rent_percent" => ($base_price)?($f_rent / $base_price) * 100:0 // % Rent
            ],
        ];
    }
}
