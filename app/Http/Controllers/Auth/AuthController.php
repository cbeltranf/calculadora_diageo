<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Traits\ApiResponse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    use ApiResponse;

    /**
     * Create a new AuthController instance.
     * @param Request $request
     * @return void
     */
    public function __construct(Request $request)
    {
        $language = $request->header('X-Language');
        App::setLocale($language ? $language : "ES");
        //
        $this->middleware('auth:api', ['except' => [
            'signup', 'login'
        ]]);
    }

    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed'
        ]);
        $user = new User();
        $dataCreate = $user->setDataCreate($request->all());
        $user = $user->create($dataCreate);
        return $this->successResponse($user, Response::HTTP_CREATED);
    }

    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);
        $credentials = request(['email', 'password']);
        if (!Auth::attempt($credentials)) {
            return $this->errorResponse('Unauthorized', Response::HTTP_UNAUTHORIZED);
        }
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->save();
        $data = $this->respondWithToken($tokenResult);
        return $this->successResponse($data, Response::HTTP_OK);
    }

    /**
     * Get the token array structure.
     *
     * @param string $token
     * @param mixed $tokenResult
     * @return array
     */
    protected function respondWithToken($tokenResult)
    {
        return [
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString(),
            'user' => \auth()->user()
        ];
    }

    /**
     * Logout user (Revoke the token)
     * @param Request $request
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return $this->successResponse("Successfully logged out", Response::HTTP_OK);
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function me()
    {
        return $this->successResponse(auth()->user(), Response::HTTP_OK);
    }
}
