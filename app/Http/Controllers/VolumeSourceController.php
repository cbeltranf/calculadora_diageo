<?php

namespace App\Http\Controllers;

use App\Http\Requests\VolumeSource\VolumeSourceCreateRequest;
use App\Http\Requests\VolumeSource\VolumeSourceUpdateRequest;
use App\Models\VolumeSource;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class VolumeSourceController extends Controller
{
    use ApiResponse;

    /**
     * @var VolumeSource
     */
    protected $volumeSource;

    /**
     * VolumeSourceController constructor.
     * @param VolumeSource $volumeSource
     */
    public function __construct(VolumeSource $volumeSource)
    {
        $this->middleware([
            'auth:api'
        ]);
        $this->volumeSource = $volumeSource;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $status = $request->status ? $request->status : "A";
        $volumeSources = VolumeSource::where('country_id', auth()->user()->country_id)
            ->where('status', $status)
            ->latest('id')
            ->get();
        return $this->successResponse($volumeSources, Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param VolumeSourceCreateRequest $volumeSourceCreateRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(VolumeSourceCreateRequest $volumeSourceCreateRequest)
    {
        $attributes = $volumeSourceCreateRequest->all();
        $dataCreate = $this->volumeSource->setDataCreate($attributes);
        $this->volumeSource = $this->volumeSource->create($dataCreate);
        return $this->successResponse($this->volumeSource, Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param VolumeSource $volumeSource
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(VolumeSource $volumeSource)
    {
        return $this->successResponse($volumeSource, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param VolumeSourceUpdateRequest $volumeSourceUpdateRequest
     * @param VolumeSource $volumeSource
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(VolumeSourceUpdateRequest $volumeSourceUpdateRequest, VolumeSource $volumeSource)
    {
        $attributes = $volumeSourceUpdateRequest->all();
        $volumeSource = $volumeSource->setDataUpdate($attributes);
        if ($volumeSource->isClean()) {
            return $this->responseIsClean($volumeSourceUpdateRequest);
        }
        $volumeSource->save();
        return $this->successResponse($volumeSource, Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     * @param VolumeSource $volumeSource
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(VolumeSource $volumeSource)
    {
        $volumeSource->delete();
        return $this->successResponse($volumeSource, Response::HTTP_OK);
    }
}
