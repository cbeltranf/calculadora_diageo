<?php

namespace App\Http\Controllers;

use App\Models\Stratum;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class StratumController extends Controller
{
    use ApiResponse;

    public function __construct()
    {
        $this->middleware([
            'auth:api'
        ]);
    }

    public function index()
    {
        $strata = Stratum::where('country_id', auth()->user()->country_id)
            ->select('id', 'name')
            ->get();
        return $this->successResponse($strata, Response::HTTP_OK);
    }
}
