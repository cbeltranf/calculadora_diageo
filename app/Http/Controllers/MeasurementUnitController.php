<?php

namespace App\Http\Controllers;

use App\Http\Requests\MeasurementUnit\MeasurementUnitCreateRequest;
use App\Http\Requests\MeasurementUnit\MeasurementUnitUpdateRequest;
use App\Models\MeasurementUnit;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class MeasurementUnitController extends Controller
{
    use ApiResponse;

    /**
     * @var MeasurementUnit
     */
    protected $measurementUnit;

    /**
     * MeasurementUnitController constructor.
     * @param MeasurementUnit $measurementUnit
     */
    public function __construct(MeasurementUnit $measurementUnit)
    {
        $this->middleware([
            'auth:api'
        ]);
        $this->measurementUnit = $measurementUnit;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $status = $request->status ? $request->status : "A";
        $measurementUnits = MeasurementUnit::where('country_id', auth()->user()->country_id)
            ->where('status', $status)
            ->latest('id')
            ->get();
        return $this->successResponse($measurementUnits, Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param MeasurementUnitCreateRequest $measurementUnitCreateRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(MeasurementUnitCreateRequest $measurementUnitCreateRequest)
    {
        $attributes = $measurementUnitCreateRequest->all();
        $dataCreate = $this->measurementUnit->setDataCreate($attributes);
        $this->measurementUnit = $this->measurementUnit->create($dataCreate);
        return $this->successResponse($this->measurementUnit, Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param MeasurementUnit $measurementUnit
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(MeasurementUnit $measurementUnit)
    {
        return $this->successResponse($measurementUnit, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param MeasurementUnitUpdateRequest $measurementUnitUpdateRequest
     * @param MeasurementUnit $measurementUnit
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(MeasurementUnitUpdateRequest $measurementUnitUpdateRequest, MeasurementUnit $measurementUnit)
    {
        $attributes = $measurementUnitUpdateRequest->all();
        $measurementUnit = $measurementUnit->setDataUpdate($attributes);
        if ($measurementUnit->isClean()) {
            return $this->responseIsClean($measurementUnitUpdateRequest);
        }
        $measurementUnit->save();
        return $this->successResponse($measurementUnit, Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     * @param MeasurementUnit $measurementUnit
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(MeasurementUnit $measurementUnit)
    {
        $measurementUnit->delete();
        return $this->successResponse($measurementUnit, Response::HTTP_OK);
    }
}
