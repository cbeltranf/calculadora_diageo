<?php

namespace App\Http\Controllers;

use App\Http\Requests\Segment\SegmentCreateRequest;
use App\Http\Requests\Segment\SegmentUpdateRequest;
use App\Models\Segment;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SegmentController extends Controller
{
    use ApiResponse;

    /**
     * @var Segment
     */
    protected $segment;

    /**
     * SegmentController constructor.
     * @param Segment $segment
     */
    public function __construct(Segment $segment)
    {
        $this->middleware([
            'auth:api'
        ]);
        $this->segment = $segment;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $status = $request->status ? $request->status : "A";
        $segments = Segment::where('country_id', auth()->user()->country_id)
            ->where('status', $status)
            ->latest('id')
            ->get();
        return $this->successResponse($segments, Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SegmentCreateRequest $segmentCreateRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(SegmentCreateRequest $segmentCreateRequest)
    {
        $attributes = $segmentCreateRequest->all();
        $dataCreate = $this->segment->setDataCreate($attributes);
        $this->segment = $this->segment->create($dataCreate);
        return $this->successResponse($this->segment, Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param Segment $segment
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Segment $segment)
    {
        return $this->successResponse($segment, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SegmentUpdateRequest $segmentUpdateRequest
     * @param Segment $segment
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(SegmentUpdateRequest $segmentUpdateRequest, Segment $segment)
    {
        $attributes = $segmentUpdateRequest->all();
        $segment = $segment->setDataUpdate($attributes);
        if ($segment->isClean()) {
            return $this->responseIsClean($segmentUpdateRequest);
        }
        $segment->save();
        return $this->successResponse($segment, Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     * @param Segment $segment
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Segment $segment)
    {
        $segment->delete();
        return $this->successResponse($segment, Response::HTTP_OK);
    }
}
