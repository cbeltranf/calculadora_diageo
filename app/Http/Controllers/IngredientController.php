<?php

namespace App\Http\Controllers;

use App\Http\Requests\Ingredient\IngredientCreateRequest;
use App\Http\Requests\Ingredient\IngredientUpdateRequest;
use App\Models\Ingredient;
use App\Models\Product;
use App\Traits\ApiResponse;
use Illuminate\Http\Response;

class IngredientController extends Controller
{
    use ApiResponse;

    /**
     * @var Ingredient
     */
    protected $ingredient;

    /**
     * IngredientController constructor.
     * @param Ingredient $ingredient
     */
    public function __construct(Ingredient $ingredient)
    {
        $this->middleware([
            'auth:api'
        ]);
        $this->ingredient = $ingredient;
    }

    /**
     * Display a listing of the resource.
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexProduct(Product $product)
    {
        $ingredients = $product->ingredients()
            ->where('status', '=', 'A')
            ->get();
        $ingredients = $this->showAllApp($ingredients);
        return $this->successResponse($ingredients, Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param IngredientCreateRequest $ingredientCreateRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(IngredientCreateRequest $ingredientCreateRequest)
    {
        $attributes = $ingredientCreateRequest->all();
        $dataCreate = $this->ingredient->setDataCreate($attributes);
        $this->ingredient = $this->ingredient->create($dataCreate);
        return $this->successResponse($this->ingredient, Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param Ingredient $ingredient
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Ingredient $ingredient)
    {
        return $this->successResponse($ingredient, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param IngredientUpdateRequest $ingredientUpdateRequest
     * @param Ingredient $ingredient
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(IngredientUpdateRequest $ingredientUpdateRequest, Ingredient $ingredient)
    {
        $attributes = $ingredientUpdateRequest->all();
        $ingredient = $ingredient->setDataUpdate($attributes);
        if ($ingredient->isClean()) {
            return $this->responseIsClean($ingredientUpdateRequest);
        }
        $ingredient->save();
        return $this->successResponse($ingredient, Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     * @param Ingredient $ingredient
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Ingredient $ingredient)
    {
        $ingredient->delete();
        return $this->successResponse($ingredient, Response::HTTP_OK);
    }

    public function showAllApp($ingredients)
    {
        $data = [];
        foreach ($ingredients as $ingredient) {
            $measurementUnit = $ingredient->measurementUnit->first();
            array_push($data, [
                'id' => $ingredient->id,
                'name' => $ingredient->name,
                'quantity' => $ingredient->quantity,
                'price_unit' => $ingredient->price_unit,
                'price_total' => $ingredient->price_unit ? $ingredient->price_unit * $ingredient->quantity : null,
                'measurement_unit' => [
                    'id' => $measurementUnit->id,
                    'name' => $measurementUnit->name,
                    'abbreviation' => $measurementUnit->abbreviation,
                ],
            ]);
        }
        return $data;
    }
}
