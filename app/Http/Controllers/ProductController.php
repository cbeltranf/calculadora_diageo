<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\ProductCreateRequest;
use App\Http\Requests\Product\ProductUpdateRequest;
use App\Models\Brand;
use App\Models\Product;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProductController extends Controller
{
    use ApiResponse;

    /**
     * @var Product
     */
    protected $product;

    /**
     * ProductController constructor.
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->middleware([
            'auth:api'
        ]);
        $this->product = $product;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $status = $request->status ? $request->status : "A";
        $products = Product::where('status', $status);
        if ($request->brand) {
            $brand = Brand::findOrFail($request->brand);
            $products = $products->where('brand_id', $brand->id);
        }
        $products = $products->latest('id')->get();
        return $this->successResponse($products, Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductCreateRequest $productCreateRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ProductCreateRequest $productCreateRequest)
    {
        $attributes = $productCreateRequest->all();
        $dataCreate = $this->product->setDataCreate($attributes);
        $this->product = $this->product->create($dataCreate);
        return $this->successResponse($this->product, Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Product $product)
    {
        return $this->successResponse($product, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProductUpdateRequest $productUpdateRequest
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ProductUpdateRequest $productUpdateRequest, Product $product)
    {
        $attributes = $productUpdateRequest->all();
        $product = $product->setDataUpdate($attributes);
        if ($product->isClean()) {
            return $this->responseIsClean($productUpdateRequest);
        }
        $product->save();
        return $this->successResponse($product, Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return $this->successResponse($product, Response::HTTP_OK);
    }
}
