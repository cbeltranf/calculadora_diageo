<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\State;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CityController extends Controller
{
    use ApiResponse;

    /**
     * @var City
     */
    protected $city;

    /**
     * CityController constructor.
     * @param City $city
     */
    public function __construct(City $city)
    {
        $this->middleware([
            'auth:api'
        ]);
        $this->city = $city;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $arrayStates = State::where('country_id', auth()->user()->country_id)
            ->pluck('id')
            ->toArray();
        $cities = City::whereIn('state_id', $arrayStates)
            ->oldest('name')
            ->get();
        return $this->successResponse($cities, Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
