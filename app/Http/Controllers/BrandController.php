<?php

namespace App\Http\Controllers;

use App\Http\Requests\Brand\BrandCreateRequest;
use App\Http\Requests\Brand\BrandUpdateRequest;
use App\Models\Brand;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BrandController extends Controller
{
    use ApiResponse;

    /**
     * @var Brand
     */
    protected $brand;

    /**
     * BrandController constructor.
     * @param Brand $brand
     */
    public function __construct(Brand $brand)
    {
        $this->middleware([
            'auth:api'
        ]);
        $this->brand = $brand;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $status = $request->status ? $request->status : "A";
        $brands = Brand::where('status', $status);
        if ($request->volumeSource) {
            $brands = $brands->where('volume_source_id', $request->volumeSource);
        }
        $brands = $brands->latest('id')->get();
        return $this->successResponse($brands, Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BrandCreateRequest $brandCreateRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(BrandCreateRequest $brandCreateRequest)
    {
        $attributes = $brandCreateRequest->all();
        $dataCreate = $this->brand->setDataCreate($attributes);
        $this->brand = $this->brand->create($dataCreate);
        return $this->successResponse($this->brand, Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param Brand $brand
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Brand $brand)
    {
        return $this->successResponse($brand, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BrandUpdateRequest $brandUpdateRequest
     * @param Brand $brand
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(BrandUpdateRequest $brandUpdateRequest, Brand $brand)
    {
        $attributes = $brandUpdateRequest->all();
        $brand = $brand->setDataUpdate($attributes);
        if ($brand->isClean()) {
            return $this->responseIsClean($brandUpdateRequest);
        }
        $brand->save();
        return $this->successResponse($brand, Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     * @param Brand $brand
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Brand $brand)
    {
        $brand->delete();
        return $this->successResponse($brand, Response::HTTP_OK);
    }
}
