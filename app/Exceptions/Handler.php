<?php

namespace App\Exceptions;

use App\Traits\ApiResponse;
use Dotenv\Exception\ValidationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    use ApiResponse;

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
         if ($exception instanceof \Illuminate\Validation\ValidationException) {
             return $this->convertValidationExceptionToResponse($exception, $request);
         }
        if ($exception instanceof AuthenticationException) {
            return $this->errorResponse("You must login", Response::HTTP_UNAUTHORIZED);
        }
        if ($exception instanceof ModelNotFoundException) {
            return $this->showMessage("The data with the specified id does not exist", Response::HTTP_NOT_FOUND);
        }
        if ($exception instanceof NotFoundHttpException) {
            return $this->errorResponse('The route does not exist', Response::HTTP_NOT_FOUND);
        }
        if ($exception instanceof MethodNotAllowedHttpException) {
            return $this->errorResponse('The method does not exist', Response::HTTP_NOT_FOUND);
        }
        return parent::render($request, $exception);
    }

     public function convertValidationExceptionToResponse(\Illuminate\Validation\ValidationException $e, $request)
     {
         $errors = $e->validator->errors()->getMessages();
         return $this->errorResponse($errors, Response::HTTP_UNPROCESSABLE_ENTITY);
     }
}
