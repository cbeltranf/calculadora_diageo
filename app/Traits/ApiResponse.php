<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

trait ApiResponse
{
    /**
     * Success response
     *
     * @param $data
     * @param integer $code
     * @return \Illuminate\Http\JsonResponse
     */
    protected function successResponse($data, $code)
    {
        return response()->json(['code' => $code, 'data' => $data], $code, ['Accept' => 'application/json']);
    }

    /**
     * Error response
     *
     * @param string $message
     * @param integer $code
     * @return \Illuminate\Http\JsonResponse
     */
    protected function errorResponse($message, $code)
    {
        return response()->json(['code' => $code, 'data' => $message], $code, ['Accept' => 'application/json']);
    }

    protected function responseIsClean(Request $request)
    {
        $language = $request->header('X-Language') ? $request->header('X-Language') : "ES";
        $code = Response::HTTP_BAD_REQUEST;
        $message = "Debe cambiar al menos un dato.";
        if ($language == "EN") {
            $message = "You must change at least one data.";
        }
        return response()->json(['code' => $code, 'data' => $message], $code, ['Accept' => 'application/json']);
    }

    /**
     * Show all
     *
     * @param Collection $collection
     * @param integer $code
     * @return \Illuminate\Http\JsonResponse
     */
    protected function showAll(Collection $collection, $code = 200)
    {
        if ($collection->isEmpty()) {
            return $this->successResponse([], $code);
        }
        $transformer = $collection->first()->transformer;
        $collection = $this->transformData($collection, $transformer);
        return $this->successResponse($collection["data"], $code);
    }

    /**
     * Show one
     *
     * @param Model $instance
     * @param integer $code
     * @return \Illuminate\Http\JsonResponse
     */
    protected function showOne(Model $instance, $code)
    {
        $transformer = $instance->transformer;
        $instance = $this->transformData($instance, $transformer);
        return $this->successResponse($instance["data"], $code);
    }

    /**
     * Show all detail
     *
     * @param Collection $collection
     * @param integer $code
     * @return \Illuminate\Http\JsonResponse
     */
    protected function showAllDetail(Collection $collection, $code)
    {
        if ($collection->isEmpty()) {
            return $this->successResponse([], $code);
        }
        $transformer = $collection->first()->detailTransformer;
        $collection = $this->transformData($collection, $transformer);
        return $this->successResponse($collection["data"], $code);
    }

    /**
     * Show one detail
     *
     * @param Model $instance
     * @param integer $code
     * @return \Illuminate\Http\JsonResponse
     */
    protected function showOneDetail(Model $instance, $code)
    {
        $transformer = $instance->detailTransformer;
        $instance = $this->transformData($instance, $transformer);
        return $this->successResponse($instance["data"], $code);
    }

    /**
     * Show all admin
     *
     * @param Collection $collection
     * @param integer $code
     * @return \Illuminate\Http\JsonResponse
     */
    protected function showAllAdmin(Collection $collection, $code)
    {
        if ($collection->isEmpty()) {
            return $this->successResponse([], $code);
        }
        $transformer = $collection->first()->adminTransformer;
        $collection = $this->transformData($collection, $transformer);
        return $this->successResponse($collection["data"], $code);
    }

    /**
     * Show one admin
     *
     * @param Model $instance
     * @param integer $code
     * @return \Illuminate\Http\JsonResponse
     */
    protected function showOneAdmin(Model $instance, $code)
    {
        $transformer = $instance->adminTransformer;
        $instance = $this->transformData($instance, $transformer);
        return $this->successResponse($instance["data"], $code);
    }

    /**
     * Show message
     *
     * @param string $message
     * @param integer $code
     * @return \Illuminate\Http\JsonResponse
     */
    protected function showMessage($message, $code)
    {
        return $this->successResponse($message, $code);
    }

    /**
     * Tranform data
     *
     * @param [type] $data
     * @param [type] $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    protected function transformData($data, $transformer)
    {
        //$transformation = fractal($data, new $transformer);
        //return $transformation->toArray();
    }
}
