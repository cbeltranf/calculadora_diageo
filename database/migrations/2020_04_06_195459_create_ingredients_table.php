<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIngredientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingredients', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('quantity');
            $table->float('price_unit', 17, 2)->nullable();
            $table->float('purchase_price', 17, 2)->nullable();
            $table->float('purchase_quantity', 17, 2)->nullable();
            $table->enum('status', ['A', 'C'])->default('A');
            $table->enum('product_diageo', ['Y', 'N'])->default('N');
            $table->unsignedBigInteger('measurement_unit_id');
            $table->foreign('measurement_unit_id')
                ->references('id')
                ->on('measurement_units');
            $table->unsignedBigInteger('product_id');
            $table->foreign('product_id')
                ->references('id')
                ->on('products');
            $table->unsignedBigInteger('country_id');
            $table->foreign('country_id')
                ->references('id')
                ->on('countries');
            $table->timestamps();
            $table->softDeletes();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ingredients');
    }
}
