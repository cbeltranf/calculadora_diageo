<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('name');
            $table->char('code');
            $table->char('iso');
            $table->string('nicename');
            $table->char('iso3');
            $table->smallInteger('numcode');
            $table->integer('phonecode');
            $table->enum('status', ['A', 'C'])->default('A');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
