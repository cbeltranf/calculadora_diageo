<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisitsTaxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visits_taxes', function (Blueprint $table) {
            $table->id();
            $table->float('value', 17, 2);
            $table->unsignedBigInteger('visit_id');
            $table->foreign('visit_id')
                ->references('id')
                ->on('visits');
            $table->unsignedBigInteger('tax_id');
            $table->foreign('tax_id')
                ->references('id')
                ->on('taxes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visits_taxes');
    }
}
