<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('image');
            $table->string('sku')->nullable();
            $table->integer('capacity');
            $table->integer('ml_shot_product');
            $table->integer('quantity_shorts');
            $table->double('cocktail_sales_price', 15, 2);
            $table->text('description')->nullable();
            $table->text('preparation')->nullable();
            $table->string('video')->nullable();
            $table->string('banner')->nullable();
            $table->enum('status', ['A', 'C'])->default('A');

            $table->unsignedBigInteger('service_measurement_id');
            $table->foreign('service_measurement_id')
                ->references('id')
                ->on('service_measurements');

            $table->unsignedBigInteger('brand_id');
            $table->foreign('brand_id')
                ->references('id')
                ->on('brands');
            $table->unsignedBigInteger('measurement_unit_id');
            $table->foreign('measurement_unit_id')
                ->references('id')
                ->on('measurement_units');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')
                ->references('id')
                ->on('users');
            $table->unsignedBigInteger('country_id');
            $table->foreign('country_id')
                ->references('id')
                ->on('countries');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
