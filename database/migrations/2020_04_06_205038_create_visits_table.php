<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visits', function (Blueprint $table) {
            $table->id();
            $table->enum('type_result', ['Bottle', 'Shot', 'Both'])->default('Bottle');
            $table->float('cost_volume_source', 17, 2)->nullable();
            $table->float('price_volume_source', 17, 2)->nullable();
            $table->float('capacity_volume_source', 17, 2)->nullable();
            $table->float('ml_shot_volume_source', 17, 2)->nullable();
            $table->float('volume_source_sales_price_shot', 17, 2)->nullable();
            $table->float('cost_product', 17, 2)->nullable();
            $table->float('price_product', 17, 2)->nullable();
            $table->float('ml_shot_product', 17, 2)->nullable();
            $table->float('capacity_product', 17, 2);
            $table->float('quantity_shorts_product', 17, 2);
            $table->float('cocktail_sales_price_product', 17, 2)->nullable();
            $table->float('profitability_shots', 17, 2)->nullable();
            $table->float('profitability_bottle', 17, 2)->nullable();
            $table->float('profit_shots_percentage', 17, 2)->nullable();
            $table->float('profit_shots_diference', 17, 2)->nullable();
            $table->float('profit_bottle_percentage', 17, 2)->nullable();
            $table->float('profit_bottle_diference', 17, 2)->nullable();
            $table->unsignedBigInteger('volume_source_id');
            $table->foreign('volume_source_id')
                ->references('id')
                ->on('volume_sources');
            $table->unsignedBigInteger('product_id');
            $table->foreign('product_id')
                ->references('id')
                ->on('products');
            $table->unsignedBigInteger('establishment_id');
            $table->foreign('establishment_id')
                ->references('id')
                ->on('establishments');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')
                ->references('id')
                ->on('users');
            $table->unsignedBigInteger('country_id');
            $table->foreign('country_id')
                ->references('id')
                ->on('countries');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visits');
    }
}
