<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('document')->unique()->nullable();
            $table->string('login_alt')->unique()->nullable();
            $table->string('social_token')->nullable();
            $table->enum('can_notify', ['S', 'N'])->default('N');
            $table->boolean('updated_data')->default(false);
            $table->string('phone')->nullable();
            $table->char('country_phone')->nullable();
            $table->date('birthday')->nullable();
            $table->enum('session_type', ['M', 'S'])->nullable();
            $table->string('avatar')->nullable();
            $table->enum('status', ['A', 'C'])->default('A');
            $table->enum('language', ['EN', 'ES'])->nullable();
            $table->dateTime('first_login')->nullable();
            $table->dateTime('last_login')->nullable();
            $table->string('platform')->nullable();
            $table->string('model')->nullable();
            $table->string('version')->nullable();
            $table->string('uuid')->nullable();
            $table->dateTime('date_password_change')->nullable();
            $table->string('recovery_code')->nullable();
            $table->unsignedBigInteger('country_id');
            $table->foreign('country_id')
                ->references('id')
                ->on('countries');
            $table->unsignedBigInteger('city_id')->nullable();
            $table->foreign('city_id')
                ->references('id')
                ->on('cities');
            $table->unsignedBigInteger('profile_id')->nullable();
            $table->foreign('profile_id')
                ->references('id')
                ->on('profiles');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
