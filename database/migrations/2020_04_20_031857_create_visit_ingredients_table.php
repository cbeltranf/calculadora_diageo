<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisitIngredientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visit_ingredients', function (Blueprint $table) {
            $table->id();
            $table->enum('product_diageo', ['Y', 'N'])->default('N');
            $table->integer('quantity');
            $table->float('price_unit', 17, 2)->nullable();
            $table->float('purchase_price', 17, 2)->nullable();
            $table->float('purchase_quantity', 17, 2)->nullable();
            $table->foreignId('ingredient_id')->constrained();
            $table->foreignId('visit_id')->constrained();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visit_ingredients');
    }
}
