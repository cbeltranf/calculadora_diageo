<?php

use Illuminate\Database\Seeder;

class BrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $img = 'https://s4.eestatic.com/2015/01/22/cocinillas/Cocinillas_5259499_115752739_1706x960.jpg';
        $names = [
            "Johnnie Walker", "Gordons", "Haig Club", "Tanqueray", "Black & White", "Old Parr",
        ];
        $volumeSourceId = \App\Models\VolumeSource::where('name', 'Cerveza Importada')->value('id');
        foreach ($names as $name) {
            $brand = new \App\Models\Brand([
'name' => $name,
                
                'image' => $img,
                'volume_source_id' => $volumeSourceId,
            ]);
            $brand->save();
        }

        $names = [
            "Zapaca", "Smirnoff", "Black & White"
        ];
        $volumeSourceId = \App\Models\VolumeSource::where('name', 'Destilado Nacional')->value('id');
        foreach ($names as $name) {
            $brand = new \App\Models\Brand([
'name' => $name,
                'image' => $img,
                'volume_source_id' => $volumeSourceId,
            ]);
            $brand->save();
        }

        $names = [
            "Don Julio", 'Tanqueray', 'Bulleit', 'Singleton', "Buchanan's", 'Old Parr'
        ];
        $volumeSourceId = \App\Models\VolumeSource::where('name', 'Destilado Importado')->value('id');
        foreach ($names as $name) {
            $brand = new \App\Models\Brand([
'name' => $name,
                'image' => $img,
                'volume_source_id' => $volumeSourceId,
            ]);
            $brand->save();
        }
    }

    
}
