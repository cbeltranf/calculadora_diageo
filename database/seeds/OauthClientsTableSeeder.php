<?php

use Illuminate\Database\Seeder;

class OauthClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = \Illuminate\Support\Facades\DB::table('oauth_clients');
        $table->truncate();
        $table->insert([
            "user_id" => null,
            "name" => "Lumen Personal Access Client",
            "secret" => "1hmKhRnUl5fJnC1Sbe1Id1lsGCRcs9vClTYjCSBL",
            "redirect" => "http://localhost",
            "personal_access_client" => true,
            "password_client" => false,
            "revoked" => false,
            "created_at" => \Carbon\Carbon::now(),
            "updated_at" => \Carbon\Carbon::now(),
        ]);
        $table->insert([
            "user_id" => null,
            "name" => "Lumen Password Grant Client",
            "secret" => "aVguFvB20XSkW1f2sdAU3wU5LkrcZabBpiuhPphd",
            "redirect" => "http://localhost",
            "personal_access_client" => false,
            "password_client" => true,
            "revoked" => false,
            "created_at" => \Carbon\Carbon::now(),
            "updated_at" => \Carbon\Carbon::now(),
        ]);

        \Illuminate\Support\Facades\DB::table('oauth_personal_access_clients')->insert([
            'client_id' => 1,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
    }
}
