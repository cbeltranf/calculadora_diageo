<?php

use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $state = new \App\Models\State([
            'name' => 'Bogotá',
            'country_id' => \App\Models\Country::where('name', 'Colombia')->value('id'),
        ]);
        $state->save();
    }
}
