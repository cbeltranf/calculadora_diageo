<?php

use Illuminate\Database\Seeder;

class MeasurementUnitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $measurementUnit = new \App\Models\MeasurementUnit([
            'name' => 'Mililitro',
            'abbreviation' => 'ML',
            'country_id' => 1
        ]);
        $measurementUnit->save();

        $measurementUnit = new \App\Models\MeasurementUnit([
            'name' => 'Gramos',
            'abbreviation' => 'GR',
            'country_id' => 1
        ]);
        $measurementUnit->save();
    }
}
