<?php

use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $city = new \App\Models\City([
            'name' => 'Bogotá',
            'state_id' => \App\Models\State::where('name', 'Bogotá')->value('id'),
        ]);
        $city->save();
    }
}
