<?php

use Illuminate\Database\Seeder;

class volumeSourceMeasurementTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $volumeSource = new \App\Models\VolumeSourceMeasurement([
            'name' => 'Botella',
        ]);
        $volumeSource->save();

        $volumeSource = new \App\Models\VolumeSourceMeasurement([
            'name' => 'Trago',
        ]);
        $volumeSource->save();
    }
}
