<?php

use Illuminate\Database\Seeder;

class SegmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = [
            "Bar Élite", "Bar Estandár", "Discoteca Élite", "Discoteca Estandár", "Licobares", "Juegos típicos",
            "Restaurante Casual", "Restaurante Élite", "Restaurante Formal"
        ];
        foreach ($names as $name) {
            $segment = new \App\Models\Segment([
                'name' => $name,
                'country_id' => 1,
            ]);
            $segment->save();
        }
    }
}
