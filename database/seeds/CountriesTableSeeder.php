<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $country = new \App\Models\Country([
            'nombre' => "Colombia",
            'name' => "Colombia",
            'code' => 'CO',
            'iso' => 'CO',
            'nicename' => 'Colombia',
            'iso3' => 'COL',
            'numcode' => '170',
            'phonecode' => '57'
        ]);
        $country->save();
    }
}
