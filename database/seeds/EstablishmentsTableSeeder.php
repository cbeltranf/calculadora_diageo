<?php

use Illuminate\Database\Seeder;

class EstablishmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $establishment = new \App\Models\Establishment([
            'name' => 'Bar la negra',
            'stratum' => 2,
            'segment_id' => 1,
            'city_id' => 1,
            'longitude' => 4.6345579,
            'latitude' => -74.0661456,
        ]);
        $establishment->save();
    }
}
