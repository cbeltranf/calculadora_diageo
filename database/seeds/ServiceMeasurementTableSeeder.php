<?php

use Illuminate\Database\Seeder;

class ServiceMeasurementTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $item = new \App\Models\ServiceMeasurement([
            'name' => 'Trago',
            'name_eng' => 'Shot',
        ]);
        $item->save();

        $item = new \App\Models\ServiceMeasurement([
            'name' => 'Botella',
            'name_eng' => 'Bottle',
        ]);
        $item->save();


    }
}
