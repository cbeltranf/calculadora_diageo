<?php

use Illuminate\Database\Seeder;

class StrataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $stratum = new \App\Models\Stratum([
            'name' => "2 y 3",
            'country_id' => 1,
        ]);
        $stratum->save();

        $stratum = new \App\Models\Stratum([
            'name' => "4, 5 y 6",
            'country_id' => 1,
        ]);
        $stratum->save();
    }
}
