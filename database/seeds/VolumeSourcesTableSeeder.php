<?php

use Illuminate\Database\Seeder;

class VolumeSourcesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $volumeSource = new \App\Models\VolumeSource([
            'name' => 'Destilado Importado',
            'country_id' => 1,
            'image' => 'https://img.freepik.com/vector-gratis/botella-vaso-whisky_1308-24939.jpg',
            'volume_source_measurement_id' => 2,
            'observations' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Praesent egestas lorem et orci volutpat efficitur.<br>In ullamcorper non turpis quis volutpat.'
        ]);
        $volumeSource->save();

        $volumeSource = new \App\Models\VolumeSource([
            'name' => 'Destilado Nacional',
            'country_id' => 1,
            'image' => 'https://img.freepik.com/vector-gratis/botella-vaso-whisky_1308-24939.jpg',
            'volume_source_measurement_id' => 1,
            'observations' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Praesent egestas lorem et orci volutpat efficitur.<br>In ullamcorper non turpis quis volutpat.'
        ]);
        $volumeSource->save();

        $volumeSource = new \App\Models\VolumeSource([
            'name' => 'Cerveza Importada',
            'country_id' => 1,
            'image' => 'https://cocinista-vsf.netdna-ssl.com/download/bancorecursos/productos2/botella-cerveza-33cl-marron-juvasa.JPG',
            'volume_source_measurement_id' => null,
            'observations' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Praesent egestas lorem et orci volutpat efficitur.<br>In ullamcorper non turpis quis volutpat.'
        ]);
        $volumeSource->save();
    }
}
