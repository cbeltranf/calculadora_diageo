<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $measurementUnit = \App\Models\MeasurementUnit::where('abbreviation', 'ML')->value('id');
        $volumeSourceId = \App\Models\VolumeSource::where('name', 'Cerveza Importada')->value('id');


        $bottle = \App\Models\ServiceMeasurement::where('name', 'Botella')->value('id');
        $shot = \App\Models\ServiceMeasurement::where('name', 'Trago')->value('id');


        $brandId = \App\Models\Brand::where('name', 'Johnnie Walker')
            ->where('volume_source_id', $volumeSourceId)
            ->value('id');
        $product = new \App\Models\Product([
            'name' => 'Johnnie Red & Lemon',
            'banner' => 'https://metrocolombiafood.vteximg.com.br/arquivos/ids/156173-750-750/5000289020701-1.jpg',
            'image' => 'https://jumbocolombiafood.vteximg.com.br/arquivos/ids/3323839-750-750/5000267014005-1.jpg',
            'capacity' => 750,
            'ml_shot_product' => 50,
            'quantity_shorts' => 12,
            'cocktail_sales_price' => 25000,
            'brand_id' => $brandId,
            'measurement_unit_id' => $measurementUnit,
            'service_measurement_id' => $shot,
            "user_id" => 1,
            "country_id" => 1,
            "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam urna elit, mattis blandit purus non, finibus porttitor nisl. Fusce posuere laoreet enim, eu maximus lectus vehicula in.",
            "preparation" => "- Llene el vaso con cubos de hielo - Vierta 50 ml de Johnnie Walker Red Label - Llene hasta el tope con gaseosa de lima - Decore con una rodaja de limón",
            "video" => '<iframe width="100%"  src="https://www.youtube.com/embed/HXNpB8QM8Es" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
        ]);
        $product->save();

        $brandId = \App\Models\Brand::where('name', 'Gordons')
            ->where('volume_source_id', $volumeSourceId)
            ->value('id');
        $product = new \App\Models\Product([
            'name' => 'Gordons Tonic',
            'banner' => 'https://metrocolombiafood.vteximg.com.br/arquivos/ids/156173-750-750/5000289020701-1.jpg',
            'image' => 'https://metrocolombiafood.vteximg.com.br/arquivos/ids/156173-750-750/5000289020701-1.jpg',

            'capacity' => 750,
            'ml_shot_product' => 50,
            'quantity_shorts' => 12,
            'cocktail_sales_price' => 25000,
            'brand_id' => $brandId,
            'measurement_unit_id' => $measurementUnit,
            'service_measurement_id' => $shot,
            "user_id" => 1,
            "country_id" => 1,
            "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam urna elit, mattis blandit purus non, finibus porttitor nisl. Fusce posuere laoreet enim, eu maximus lectus vehicula in.",
            "preparation" => "- En un vaso largo o copa globo agrega hielo hasta el tope - Luego agregue 50 ml de Gin Gordon's London Dry y complete con agua tónica - Decore con una rodaja de limón",
            "video" => '<iframe width="100%"  src="https://www.youtube.com/embed/HXNpB8QM8Es" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
        ]);
        $product->save();

        $brandId = \App\Models\Brand::where('name', 'Haig Club')
            ->where('volume_source_id', $volumeSourceId)
            ->value('id');
        $product = new \App\Models\Product([
            'name' => 'H.O.T',
            'banner' => 'https://metrocolombiafood.vteximg.com.br/arquivos/ids/156173-750-750/5000289020701-1.jpg',
            'image' => 'https://jumbocolombiafood.vteximg.com.br/arquivos/ids/3406206-750-750/5000281051116.jpg',
            'capacity' => 750,
'ml_shot_product' => 50,
            'quantity_shorts' => 12,
            'cocktail_sales_price' => 25000,
            'brand_id' => $brandId,
            'measurement_unit_id' => $measurementUnit,
            'service_measurement_id' => $shot,
            "user_id" => 1,
            "country_id" => 1,
            "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam urna elit, mattis blandit purus non, finibus porttitor nisl. Fusce posuere laoreet enim, eu maximus lectus vehicula in.",
            "preparation" => "- En una copa de vino agrega hielo hasta el borde. - Agrega 50 ml de Whisky Haig Club. - Completa con agua tónica. - Decora con una media luna de naranja",
            "video" => '<iframe width="100%"  src="https://www.youtube.com/embed/HXNpB8QM8Es" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
        ]);
        $product->save();

        $brandId = \App\Models\Brand::where('name', 'Tanqueray')
            ->where('volume_source_id', $volumeSourceId)
            ->value('id');
        $product = new \App\Models\Product([
            'name' => 'Tanqueray & Tonic',
            'banner' => 'https://metrocolombiafood.vteximg.com.br/arquivos/ids/156173-750-750/5000289020701-1.jpg',
            'image' => 'https://jumbocolombiafood.vteximg.com.br/arquivos/ids/238843-750-750/5000291020706-GINEBRA-TANQUERAY-GYN-750-ML.jpg',
            'capacity' => 750,
'ml_shot_product' => 50,
            'quantity_shorts' => 12,
            'cocktail_sales_price' => 25000,
            'brand_id' => $brandId,
            'measurement_unit_id' => $measurementUnit,
            'service_measurement_id' => $shot,
            "user_id" => 1,
            "country_id" => 1,
            "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam urna elit, mattis blandit purus non, finibus porttitor nisl. Fusce posuere laoreet enim, eu maximus lectus vehicula in.",
            "preparation" => "- En una copa globo agregue 3/4 partes de hielo. - Luego agregue 50ml de Tanqueray London Dry Gin. - Por ultimo complete con agua tónica y decore con una rodaja de naranja.",
            "video" => '<iframe width="100%"  src="https://www.youtube.com/embed/HXNpB8QM8Es" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
        ]);
        $product->save();

        $brandId = \App\Models\Brand::where('name', 'Black & White')
            ->where('volume_source_id', $volumeSourceId)
            ->value('id');
        $product = new \App\Models\Product([
            'name' => 'Trago B&W',
            'image' => "https://dislicores.vteximg.com.br/arquivos/ids/156499-1000-1000/Licores-whisky_002331_1.jpg",
            'capacity' => 750,
            'ml_shot_product' => 50,
            'quantity_shorts' => 12,
            'cocktail_sales_price' => 25000,
            'brand_id' => $brandId,
            'measurement_unit_id' => $measurementUnit,
            'service_measurement_id' => $shot,
            "user_id" => 1,
            "country_id" => 1,
            "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam urna elit, mattis blandit purus non, finibus porttitor nisl. Fusce posuere laoreet enim, eu maximus lectus vehicula in.",
            "preparation" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vitae mi sollicitudin, tempor massa ac, efficitur lacus. Integer non mollis nulla. Aenean ut urna varius, efficitur enim at, mattis tellus. Nam interdum massa at est ultricies pretium.",
            "video" => '<iframe width="100%"  src="https://www.youtube.com/embed/HXNpB8QM8Es" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
        ]);
        $product->save();

        $brandId = \App\Models\Brand::where('name', 'Old Parr')
            ->where('volume_source_id', $volumeSourceId)
            ->value('id');
        $product = new \App\Models\Product([
            'name' => 'Old Parr Beer',
            'image' => "https://jumbocolombiafood.vteximg.com.br/arquivos/ids/3393605-750-750/5000281055084.jpg",
            'capacity' => 750,
'ml_shot_product' => 50,
            'quantity_shorts' => 12,
            'cocktail_sales_price' => 25000,
            'brand_id' => $brandId,
            'measurement_unit_id' => $measurementUnit,
            'service_measurement_id' => $shot,
            "user_id" => 1,
            "country_id" => 1,
            "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam urna elit, mattis blandit purus non, finibus porttitor nisl. Fusce posuere laoreet enim, eu maximus lectus vehicula in.",
            "preparation" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vitae mi sollicitudin, tempor massa ac, efficitur lacus. Integer non mollis nulla. Aenean ut urna varius, efficitur enim at, mattis tellus. Nam interdum massa at est ultricies pretium.",
            "video" => '<iframe width="100%"  src="https://www.youtube.com/embed/HXNpB8QM8Es" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
        ]);
        $product->save();

        $volumeSourceId = \App\Models\VolumeSource::where('name', 'Destilado Nacional')->value('id');
        $brandId = \App\Models\Brand::where('name', 'Zapaca')
            ->where('volume_source_id', $volumeSourceId)
            ->value('id');
        $product = new \App\Models\Product([
            'name' => 'Mojito Zacapa Ambar',
            'banner' => 'https://metrocolombiafood.vteximg.com.br/arquivos/ids/156173-750-750/5000289020701-1.jpg',
            'image' => 'https://d26lpennugtm8s.cloudfront.net/stores/098/244/products/zapacaxonavidad1-d1d82e631c9c8d8b5215122049296474-640-0.jpg',
            'capacity' => 750,
'ml_shot_product' => 50,
            'quantity_shorts' => 12,
            'cocktail_sales_price' => 25000,
            'brand_id' => $brandId,
            'measurement_unit_id' => $measurementUnit,
            'service_measurement_id' => $shot,
            "user_id" => 1,
            "country_id" => 1,
            "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam urna elit, mattis blandit purus non, finibus porttitor nisl. Fusce posuere laoreet enim, eu maximus lectus vehicula in.",
            "preparation" => "- En un vaso largo agrega hielo hasta el borde. - Lugeo agrega 50ml de Ron Zacapa Ambar 30ml de zumo de limón y 30ml de almíba, mezcla todo esto bien con unas hojas de hierbabuena. - por ultimo decora con una rama de hierbabuena.",
            "video" => '<iframe width="100%"  src="https://www.youtube.com/embed/HXNpB8QM8Es" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
        ]);
        $product->save();

        $brandId = \App\Models\Brand::where('name', 'Smirnoff')
            ->where('volume_source_id', $volumeSourceId)
            ->value('id');
        $product = new \App\Models\Product([
            'name' => 'Botll Smirnoff x1',
            'banner' => 'https://metrocolombiafood.vteximg.com.br/arquivos/ids/156173-750-750/5000289020701-1.jpg',
            'image' => 'https://metrocolombiafood.vteximg.com.br/arquivos/ids/228274-1000-1000/5410316951821.jpg',
            'capacity' => 750,
'ml_shot_product' => 50,
            'quantity_shorts' => 1,
            'cocktail_sales_price' => 25000,
            'brand_id' => $brandId,
            'measurement_unit_id' => $measurementUnit,
            'service_measurement_id' => $shot, //'service_measurement_id' => $bottle,
            "user_id" => 1,
            "country_id" => 1,
            "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam urna elit, mattis blandit purus non, finibus porttitor nisl. Fusce posuere laoreet enim, eu maximus lectus vehicula in.",
            "preparation" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vitae mi sollicitudin, tempor massa ac, efficitur lacus. Integer non mollis nulla. Aenean ut urna varius, efficitur enim at, mattis tellus. Nam interdum massa at est ultricies pretium.",
            "video" => '<iframe width="100%"  src="https://www.youtube.com/embed/HXNpB8QM8Es" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
        ]);
        $product->save();

        $brandId = \App\Models\Brand::where('name', 'Black & White')
            ->where('volume_source_id', $volumeSourceId)
            ->value('id');
        $product = new \App\Models\Product([
            'name' => 'Botll B&W',
            'image' => "https://dislicores.vteximg.com.br/arquivos/ids/156499-1000-1000/Licores-whisky_002331_1.jpg",
            'capacity' => 750,
'ml_shot_product' => 50,
            'quantity_shorts' => 12,
            'cocktail_sales_price' => 25000,
            'brand_id' => $brandId,
            'measurement_unit_id' => $measurementUnit,
            'service_measurement_id' => $shot, //'service_measurement_id' => $bottle,
            "user_id" => 1,
            "country_id" => 1,
            "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam urna elit, mattis blandit purus non, finibus porttitor nisl. Fusce posuere laoreet enim, eu maximus lectus vehicula in.",
            "preparation" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vitae mi sollicitudin, tempor massa ac, efficitur lacus. Integer non mollis nulla. Aenean ut urna varius, efficitur enim at, mattis tellus. Nam interdum massa at est ultricies pretium.",
            "video" => '<iframe width="100%"  src="https://www.youtube.com/embed/HXNpB8QM8Es" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
        ]);
        $product->save();

        $volumeSourceId = \App\Models\VolumeSource::where('name', 'Destilado Importado')->value('id');
        $brandId = \App\Models\Brand::where('name', 'Don Julio')
            ->where('volume_source_id', $volumeSourceId)
            ->value('id');
        $product = new \App\Models\Product([
            'name' => 'Lupe Don Julio Blanco',
            'banner' => 'https://metrocolombiafood.vteximg.com.br/arquivos/ids/156173-750-750/5000289020701-1.jpg',
            'image' => 'https://http2.mlstatic.com/tequila-don-julio-reposado-700-ml-D_NQ_NP_814843-MLM31695261190_082019-F.jpg',
            'capacity' => 750,
'ml_shot_product' => 50,
            'quantity_shorts' => 12,
            'cocktail_sales_price' => 25000,
            'brand_id' => $brandId,
            'measurement_unit_id' => $measurementUnit,
            'service_measurement_id' => $shot,
            "user_id" => 1,
            "country_id" => 1,
            "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam urna elit, mattis blandit purus non, finibus porttitor nisl. Fusce posuere laoreet enim, eu maximus lectus vehicula in.",
            "preparation" => "En una coctelera con hielo agregue 30 ml de zumo de limón, 30 ml de almíbar simple, y los 50 ml de Don Julio Blanco. - Agitar enérgicamente por 10 segundos y servir en un vaso con hielo. - Decorar con rodaja de limón.",
            "video" => '<iframe width="100%"  src="https://www.youtube.com/embed/HXNpB8QM8Es" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
        ]);
        $product->save();

        $brandId = \App\Models\Brand::where('name', 'Don Julio')
            ->where('volume_source_id', $volumeSourceId)
            ->value('id');
        $product = new \App\Models\Product([
            'name' => 'Paloma Don Julio Reposado',
            'banner' => 'https://metrocolombiafood.vteximg.com.br/arquivos/ids/156173-750-750/5000289020701-1.jpg',
            'image' => 'https://http2.mlstatic.com/tequila-don-julio-reposado-700-ml-D_NQ_NP_814843-MLM31695261190_082019-F.jpg',
            'capacity' => 750,
'ml_shot_product' => 50,
            'quantity_shorts' => 12,
            'cocktail_sales_price' => 25000,
            'brand_id' => $brandId,
            'measurement_unit_id' => $measurementUnit,
            'service_measurement_id' => $shot,
            "user_id" => 1,
            "country_id" => 1,
            "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam urna elit, mattis blandit purus non, finibus porttitor nisl. Fusce posuere laoreet enim, eu maximus lectus vehicula in.",
            "preparation" => "- En un vaso largo agregue hielo hasta el tope. - Luego agregue tequila Don Julio reposado, zumo de toronja y almíbar. - Complete con agua tónica y decore con una rodaja de toronja.",
            "video" => '<iframe width="100%"  src="https://www.youtube.com/embed/HXNpB8QM8Es" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
        ]);
        $product->save();

        $brandId = \App\Models\Brand::where('name', 'Tanqueray')
            ->where('volume_source_id', $volumeSourceId)
            ->value('id');
        $product = new \App\Models\Product([
            'name' => 'Tea Ten',
            'banner' => 'https://metrocolombiafood.vteximg.com.br/arquivos/ids/156173-750-750/5000289020701-1.jpg',
            'image' => 'https://jumbocolombiafood.vteximg.com.br/arquivos/ids/238843-750-750/5000291020706-GINEBRA-TANQUERAY-GYN-750-ML.jpg',
            'capacity' => 750,
'ml_shot_product' => 50,
            'quantity_shorts' => 12,
            'cocktail_sales_price' => 25000,
            'brand_id' => $brandId,
            'measurement_unit_id' => $measurementUnit,
            'service_measurement_id' => $shot,
            "user_id" => 1,
            "country_id" => 1,
            "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam urna elit, mattis blandit purus non, finibus porttitor nisl. Fusce posuere laoreet enim, eu maximus lectus vehicula in.",
            "preparation" => "- En una copa globo agregue 3/4 partes de hielo. - Luego agregue 50ml de Tanqueray Ten, sumerja el té en la copa. - Por ultimo complete con agua tónica.",
            "video" => '<iframe width="100%"  src="https://www.youtube.com/embed/HXNpB8QM8Es" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
        ]);
        $product->save();

        $brandId = \App\Models\Brand::where('name', 'Tanqueray')
            ->where('volume_source_id', $volumeSourceId)
            ->value('id');
        $product = new \App\Models\Product([
            'name' => 'Tanqueray Rangpur & Tonic',
            'banner' => 'https://metrocolombiafood.vteximg.com.br/arquivos/ids/156173-750-750/5000289020701-1.jpg',
            'image' => 'https://jumbocolombiafood.vteximg.com.br/arquivos/ids/238843-750-750/5000291020706-GINEBRA-TANQUERAY-GYN-750-ML.jpg',
            'capacity' => 750,
'ml_shot_product' => 50,
            'quantity_shorts' => 12,
            'cocktail_sales_price' => 25000,
            'brand_id' => $brandId,
            'measurement_unit_id' => $measurementUnit,
            'service_measurement_id' => $shot,
            "user_id" => 1,
            "country_id" => 1,
            "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam urna elit, mattis blandit purus non, finibus porttitor nisl. Fusce posuere laoreet enim, eu maximus lectus vehicula in.",
            "preparation" => "- En una copa globo agregue 3/4 partes de hielo. - Luego agregue 50ml de Tanqueray Rangpur. - Por ultimo complete con agua tónica y decore con una rodaja de naranja.",
            "video" => '<iframe width="100%"  src="https://www.youtube.com/embed/HXNpB8QM8Es" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
        ]);
        $product->save();

        $brandId = \App\Models\Brand::where('name', 'Tanqueray')
            ->where('volume_source_id', $volumeSourceId)
            ->value('id');
        $product = new \App\Models\Product([
            'name' => 'Tanqueray & Tonic',
            'banner' => 'https://metrocolombiafood.vteximg.com.br/arquivos/ids/156173-750-750/5000289020701-1.jpg',
            'image' => 'https://jumbocolombiafood.vteximg.com.br/arquivos/ids/238843-750-750/5000291020706-GINEBRA-TANQUERAY-GYN-750-ML.jpg',
            'capacity' => 750,
'ml_shot_product' => 50,
            'quantity_shorts' => 12,
            'cocktail_sales_price' => 25000,
            'brand_id' => $brandId,
            'measurement_unit_id' => $measurementUnit,
            'service_measurement_id' => $shot,
            "user_id" => 1,
            "country_id" => 1,
            "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam urna elit, mattis blandit purus non, finibus porttitor nisl. Fusce posuere laoreet enim, eu maximus lectus vehicula in.",
            "preparation" => "- En una copa globo agregue 3/4 partes de hielo. - Luego agregue 50ml de Tanqueray London Dry Gin. - Por ultimo complete con agua tónica y decore con una rodaja de naranja.",
            "video" => '<iframe width="100%"  src="https://www.youtube.com/embed/HXNpB8QM8Es" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
        ]);
        $product->save();

        $brandId = \App\Models\Brand::where('name', 'Bulleit')
            ->where('volume_source_id', $volumeSourceId)
            ->value('id');
        $product = new \App\Models\Product([
            'name' => 'Bulleit & Beer',
            'banner' => 'https://metrocolombiafood.vteximg.com.br/arquivos/ids/156173-750-750/5000289020701-1.jpg',
            'image' => 'https://products2.imgix.drizly.com/ci-bulleit-bourbon-07be0e5c0084bc44.jpeg',
            'capacity' => 750,
'ml_shot_product' => 50,
            'quantity_shorts' => 12,
            'cocktail_sales_price' => 25000,
            'brand_id' => $brandId,
            'measurement_unit_id' => $measurementUnit,
            'service_measurement_id' => $shot,
            "user_id" => 1,
            "country_id" => 1,
            "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam urna elit, mattis blandit purus non, finibus porttitor nisl. Fusce posuere laoreet enim, eu maximus lectus vehicula in.",
            "preparation" => "- En un vaso largo agregue hielo hasta el tope. - Luego agregue 30ml de whisky Bulleit Bourbon y completa con cerveza. - Decore con una rodaja de naranja.",
            "video" => '<iframe width="100%"  src="https://www.youtube.com/embed/HXNpB8QM8Es" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
        ]);
        $product->save();

        $brandId = \App\Models\Brand::where('name', 'Singleton')
            ->where('volume_source_id', $volumeSourceId)
            ->value('id');
        $product = new \App\Models\Product([
            'name' => 'Singleton Penicilin',
            'banner' => 'https://metrocolombiafood.vteximg.com.br/arquivos/ids/156173-750-750/5000289020701-1.jpg',
            'image' => 'https://www.malts.com/media/4758992/6226061_diageo_the_singleton_700ml_bottle_dufftown_whisky_25yo_front.jpg',
            'capacity' => 750,
'ml_shot_product' => 50,
            'quantity_shorts' => 12,
            'cocktail_sales_price' => 25000,
            'brand_id' => $brandId,
            'measurement_unit_id' => $measurementUnit,
            'service_measurement_id' => $shot,
            "user_id" => 1,
            "country_id" => 1,
            "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam urna elit, mattis blandit purus non, finibus porttitor nisl. Fusce posuere laoreet enim, eu maximus lectus vehicula in.",
            "preparation" => "- En un vaso corto agregue 3/4 partes hielo. - Luego agregue a la coctelera, 45ml de Whisky Singleton 12, zumo de limón, zumo de jengibre, la miel Y mézclelo bien. - Para finalizar sirva al vaso y agregue unas gotas de solución salina.",
            "video" => '<iframe width="100%"  src="https://www.youtube.com/embed/HXNpB8QM8Es" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
        ]);
        $product->save();

        $brandId = \App\Models\Brand::where('name', 'Singleton')
            ->where('volume_source_id', $volumeSourceId)
            ->value('id');
        $product = new \App\Models\Product([
            'name' => 'Trago Singleton',
            'banner' => 'https://metrocolombiafood.vteximg.com.br/arquivos/ids/156173-750-750/5000289020701-1.jpg',
            'image' => 'https://www.malts.com/media/4758992/6226061_diageo_the_singleton_700ml_bottle_dufftown_whisky_25yo_front.jpg',
            'capacity' => 750,
'ml_shot_product' => 50,
            'quantity_shorts' => 12,
            'cocktail_sales_price' => 25000,
            'brand_id' => $brandId,
            'measurement_unit_id' => $measurementUnit,
            'service_measurement_id' => $shot,
            "user_id" => 1,
            "country_id" => 1,
            "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam urna elit, mattis blandit purus non, finibus porttitor nisl. Fusce posuere laoreet enim, eu maximus lectus vehicula in.",
            "preparation" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vitae mi sollicitudin, tempor massa ac, efficitur lacus. Integer non mollis nulla. Aenean ut urna varius, efficitur enim at, mattis tellus. Nam interdum massa at est ultricies pretium.",
            "video" => '<iframe width="100%"  src="https://www.youtube.com/embed/HXNpB8QM8Es" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
        ]);
        $product->save();

        $brandId = \App\Models\Brand::where('name', "Buchanan's")
            ->where('volume_source_id', $volumeSourceId)
            ->value('id');
        $product = new \App\Models\Product([
            'name' => "Trago Buchanan's 18",
            'image' => "https://exitocol.vtexassets.com/arquivos/ids/1270076-600-auto?width=600&height=auto&aspect=true",
            'capacity' => 750,
'ml_shot_product' => 50,
            'quantity_shorts' => 12,
            'cocktail_sales_price' => 25000,
            'brand_id' => $brandId,
            'measurement_unit_id' => $measurementUnit,
            'service_measurement_id' => $shot,
            "user_id" => 1,
            "country_id" => 1,
            "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam urna elit, mattis blandit purus non, finibus porttitor nisl. Fusce posuere laoreet enim, eu maximus lectus vehicula in.",
            "preparation" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vitae mi sollicitudin, tempor massa ac, efficitur lacus. Integer non mollis nulla. Aenean ut urna varius, efficitur enim at, mattis tellus. Nam interdum massa at est ultricies pretium.",
            "video" => '<iframe width="100%"  src="https://www.youtube.com/embed/HXNpB8QM8Es" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
        ]);
        $product->save();

        $brandId = \App\Models\Brand::where('name', 'Old Parr')
            ->where('volume_source_id', $volumeSourceId)
            ->value('id');
        $product = new \App\Models\Product([
            'name' => 'Botll Old Parr 18',
            'image' => "https://jumbocolombiafood.vteximg.com.br/arquivos/ids/3393605-750-750/5000281055084.jpg",
            'capacity' => 750,
'ml_shot_product' => 50,
            'quantity_shorts' => 12,
            'cocktail_sales_price' => 25000,
            'brand_id' => $brandId,
            'measurement_unit_id' => $measurementUnit,
            'service_measurement_id' => $shot, //'service_measurement_id' => $bottle,
            "user_id" => 1,
            "country_id" => 1,
            "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam urna elit, mattis blandit purus non, finibus porttitor nisl. Fusce posuere laoreet enim, eu maximus lectus vehicula in.",
            "preparation" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vitae mi sollicitudin, tempor massa ac, efficitur lacus. Integer non mollis nulla. Aenean ut urna varius, efficitur enim at, mattis tellus. Nam interdum massa at est ultricies pretium.",
            "video" => '<iframe width="100%"  src="https://www.youtube.com/embed/HXNpB8QM8Es" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
        ]);
        $product->save();
    }
}
