<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            OauthClientsTableSeeder::class,
            CountriesTableSeeder::class,
            StrataTableSeeder::class,
            StatesTableSeeder::class,
            CitiesTableSeeder::class,
            UsersTableSeeder::class,
            SegmentsTableSeeder::class,
            EstablishmentsTableSeeder::class,
            volumeSourceMeasurementTableSeeder::class,
            VolumeSourcesTableSeeder::class,
            BrandsTableSeeder::class,
            MeasurementUnitsTableSeeder::class,
            ServiceMeasurementTableSeeder::class,
            ProductsTableSeeder::class,
            IngredientsTableSeeder::class,
            TaxesTableSeeder::class,
        ]);
    }
}
