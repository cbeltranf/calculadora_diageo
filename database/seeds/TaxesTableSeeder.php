<?php

use Illuminate\Database\Seeder;

class TaxesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tax = new \App\Models\Tax([
            'name' => 'Imp. al Consumo',
            'value' => 8,
            'country_id' => 1
        ]);
        $tax->save();
    }
}
