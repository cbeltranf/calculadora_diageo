<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\Models\User([
            'name' => 'Admin',
            'last_name' => 'Diageo',
            'email' => 'admin@diageo.com',
            'password' => bcrypt('admin1234'),
            'country_id' => \App\Models\Country::where('name', 'Colombia')->value('id'),
        ]);
        $user->save();
    }
}
