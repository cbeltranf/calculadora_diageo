<?php

use Illuminate\Database\Seeder;

class IngredientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mlId = \App\Models\MeasurementUnit::where('abbreviation', 'ML')
            ->value('id');
        $grId = \App\Models\MeasurementUnit::where('abbreviation', 'GR')
            ->value('id');
        // Johnnie Red & Lemon
        $ingredient = new \App\Models\Ingredient([
            'name' => 'Whisky JW Red Label',
            'quantity' => 50,
            'measurement_unit_id' => $mlId,
            'product_id' => 1,
            'product_diageo' => 'Y',
            'price_unit' => null,
            'country_id' => 1,
        ]);
        $ingredient->save();
        $ingredient = new \App\Models\Ingredient([
            'name' => 'Gaseosa de Lima',
            'quantity' => 150,
            'measurement_unit_id' => $mlId,
            'product_id' => 1,
            'price_unit' => 7.14,
            'country_id' => 1,
        ]);
        $ingredient->save();
        $ingredient = new \App\Models\Ingredient([
            'name' => 'Limón Tahití',
            'quantity' => 10,
            'measurement_unit_id' => $grId,
            'product_id' => 1,
            'price_unit' => 7,
            'country_id' => 1,
        ]);
        $ingredient->save();
        // Gordons Tonic
        $ingredient = new \App\Models\Ingredient([
            'name' => 'Limón Tahití',
            'quantity' => 15,
            'measurement_unit_id' => $grId,
            'product_id' => 2,
            'price_unit' => 7,
            'country_id' => 1,
        ]);
        $ingredient->save();
        $ingredient = new \App\Models\Ingredient([
            'name' => "Gin Gordon's London Dry",
            'quantity' => 50,
            'measurement_unit_id' => $mlId,
            'product_id' => 2,
            'product_diageo' => 'Y',
            'price_unit' => null,
            'country_id' => 1,
        ]);
        $ingredient->save();
        $ingredient = new \App\Models\Ingredient([
            'name' => "Agua Tonica",
            'quantity' => 150,
            'measurement_unit_id' => $mlId,
            'product_id' => 2,
            'price_unit' => 5.45,
            'country_id' => 1,
        ]);
        $ingredient->save();
        // H.O.T
        $ingredient = new \App\Models\Ingredient([
            'name' => "Agua Tonica",
            'quantity' => 150,
            'measurement_unit_id' => $mlId,
            'product_id' => 3,
            'price_unit' => 5.45,
            'country_id' => 1,
        ]);
        $ingredient->save();
        $ingredient = new \App\Models\Ingredient([
            'name' => "Haig Club",
            'quantity' => 50,
            'measurement_unit_id' => $mlId,
            'product_id' => 3,
            'product_diageo' => 'Y',
            'price_unit' => null,
            'country_id' => 1,
        ]);
        $ingredient->save();
        $ingredient = new \App\Models\Ingredient([
            'name' => "Naranja",
            'quantity' => 25,
            'measurement_unit_id' => $grId,
            'product_id' => 3,
            'price_unit' => 5,
            'country_id' => 1,
        ]);
        $ingredient->save();
        // Tanqueray & Tonic
        $ingredient = new \App\Models\Ingredient([
            'name' => "Agua Tonica",
            'quantity' => 100,
            'measurement_unit_id' => $mlId,
            'product_id' => 4,
            'price_unit' => 5.45,
            'country_id' => 1,
        ]);
        $ingredient->save();
        $ingredient = new \App\Models\Ingredient([
            'name' => "Naranja",
            'quantity' => 10,
            'measurement_unit_id' => $grId,
            'product_id' => 4,
            'price_unit' => 5,
            'country_id' => 1,
        ]);
        $ingredient->save();
        $ingredient = new \App\Models\Ingredient([
            'name' => "Tanqueray Lodon Dry",
            'quantity' => 50,
            'measurement_unit_id' => $mlId,
            'product_id' => 4,
            'product_diageo' => 'Y',
            'price_unit' => null,
            'country_id' => 1,
        ]);
        $ingredient->save();
        // Trago B&W
        $ingredient = new \App\Models\Ingredient([
            'name' => "Black & White",
            'quantity' => 50,
            'measurement_unit_id' => $mlId,
            'product_id' => 5,
            'product_diageo' => 'Y',
            'price_unit' => null,
            'country_id' => 1,
        ]);
        $ingredient->save();
        // Old Parr Beer
        $ingredient = new \App\Models\Ingredient([
            'name' => "Limón Tahití",
            'quantity' => 15,
            'measurement_unit_id' => $grId,
            'product_id' => 6,
            'price_unit' => 7,
            'country_id' => 1,
        ]);
        $ingredient->save();
        $ingredient = new \App\Models\Ingredient([
            'name' => "Old Parr",
            'quantity' => 50,
            'measurement_unit_id' => $mlId,
            'product_id' => 6,
            'product_diageo' => 'Y',
            'price_unit' => null,
            'country_id' => 1,
        ]);
        $ingredient->save();
        $ingredient = new \App\Models\Ingredient([
            'name' => "Cerveza Dorada",
            'quantity' => 10,
            'measurement_unit_id' => $mlId,
            'product_id' => 6,
            'price_unit' => 8.79,
            'country_id' => 1,
        ]);
        $ingredient->save();
        // Mojito Zacapa Ambar
        $ingredient = new \App\Models\Ingredient([
            'name' => "Almibar Simple",
            'quantity' => 30,
            'measurement_unit_id' => $mlId,
            'product_id' => 7,
            'price_unit' => 5,
            'country_id' => 1,
        ]);
        $ingredient->save();
        $ingredient = new \App\Models\Ingredient([
            'name' => "Zumo de Limón",
            'quantity' => 30,
            'measurement_unit_id' => $mlId,
            'product_id' => 7,
            'price_unit' => 12,
            'country_id' => 1,
        ]);
        $ingredient->save();
        $ingredient = new \App\Models\Ingredient([
            'name' => "Zacapa Ambar",
            'quantity' => 50,
            'measurement_unit_id' => $mlId,
            'product_id' => 7,
            'product_diageo' => 'Y',
            'price_unit' => null,
            'country_id' => 1,
        ]);
        $ingredient->save();
        $ingredient = new \App\Models\Ingredient([
            'name' => "Hierbabuena",
            'quantity' => 10,
            'measurement_unit_id' => $grId,
            'product_id' => 7,
            'price_unit' => 10,
            'country_id' => 1,
        ]);
        $ingredient->save();
        // Botll Smirnoff x1
        $ingredient = new \App\Models\Ingredient([
            'name' => "Botll Smirnoff x1",
            'quantity' => 50,
            'measurement_unit_id' => $mlId,
            'product_id' => 8,
            'product_diageo' => 'Y',
            'price_unit' => null,
            'country_id' => 1,
        ]);
        $ingredient->save();
        // Botll B&W
        $ingredient = new \App\Models\Ingredient([
            'name' => "Black & White",
            'quantity' => 50,
            'measurement_unit_id' => $mlId,
            'product_id' => 9,
            'product_diageo' => 'Y',
            'price_unit' => null,
            'country_id' => 1,
        ]);
        $ingredient->save();
        // Lupe Don Julio Blanco
        $ingredient = new \App\Models\Ingredient([
            'name' => "Almibar Simple",
            'quantity' => 30,
            'measurement_unit_id' => $mlId,
            'product_id' => 10,
            'price_unit' => 5,
            'country_id' => 1,
        ]);
        $ingredient->save();
        $ingredient = new \App\Models\Ingredient([
            'name' => "Don Julio Blanco",
            'quantity' => 50,
            'measurement_unit_id' => $mlId,
            'product_id' => 10,
            'product_diageo' => 'Y',
            'price_unit' => null,
            'country_id' => 1,
        ]);
        $ingredient->save();
        $ingredient = new \App\Models\Ingredient([
            'name' => "Zumo de Limón",
            'quantity' => 30,
            'measurement_unit_id' => $mlId,
            'product_id' => 10,
            'price_unit' => 12,
            'country_id' => 1,
        ]);
        $ingredient->save();
        // Paloma Don Julio Reposado
        $ingredient = new \App\Models\Ingredient([
            'name' => "Agua Tonica",
            'quantity' => 90,
            'measurement_unit_id' => $mlId,
            'product_id' => 11,
            'price_unit' => 5.45,
            'country_id' => 1,
        ]);
        $ingredient->save();
        $ingredient = new \App\Models\Ingredient([
            'name' => "Almibar Simple",
            'quantity' => 10,
            'measurement_unit_id' => $mlId,
            'product_id' => 11,
            'price_unit' => 5,
            'country_id' => 1,
        ]);
        $ingredient->save();
        $ingredient = new \App\Models\Ingredient([
            'name' => "Don Julio Reposado",
            'quantity' => 45,
            'measurement_unit_id' => $mlId,
            'product_id' => 11,
            'product_diageo' => 'Y',
            'price_unit' => null,
            'country_id' => 1,
        ]);
        $ingredient->save();
        $ingredient = new \App\Models\Ingredient([
            'name' => "Zumo de Toronja",
            'quantity' => 30,
            'measurement_unit_id' => $mlId,
            'product_id' => 11,
            'price_unit' => 8,
            'country_id' => 1,
        ]);
        $ingredient->save();
        $ingredient = new \App\Models\Ingredient([
            'name' => "Toronja",
            'quantity' => 10,
            'measurement_unit_id' => $grId,
            'product_id' => 11,
            'price_unit' => 6,
            'country_id' => 1,
        ]);
        $ingredient->save();
        // Tea Ten
        $ingredient = new \App\Models\Ingredient([
            'name' => "Agua Tonica",
            'quantity' => 10,
            'measurement_unit_id' => $mlId,
            'product_id' => 12,
            'price_unit' => 5.45,
            'country_id' => 1,
        ]);
        $ingredient->save();
        $ingredient = new \App\Models\Ingredient([
            'name' => "Tanqueray Ten",
            'quantity' => 50,
            'measurement_unit_id' => $mlId,
            'product_id' => 12,
            'product_diageo' => 'Y',
            'price_unit' => null,
            'country_id' => 1,
        ]);
        $ingredient->save();
        $ingredient = new \App\Models\Ingredient([
            'name' => "Te Twinings",
            'quantity' => 2,
            'measurement_unit_id' => $grId,
            'product_id' => 12,
            'price_unit' => 500,
            'country_id' => 1,
        ]);
        $ingredient->save();
        // Tanqueray Rangpur & Tonic
        $ingredient = new \App\Models\Ingredient([
            'name' => "Agua Tonica",
            'quantity' => 10,
            'measurement_unit_id' => $mlId,
            'product_id' => 13,
            'price_unit' => 5.45,
            'country_id' => 1,
        ]);
        $ingredient->save();
        $ingredient = new \App\Models\Ingredient([
            'name' => "Naranja",
            'quantity' => 10,
            'measurement_unit_id' => $grId,
            'product_id' => 13,
            'price_unit' => 5,
            'country_id' => 1,
        ]);
        $ingredient->save();
        $ingredient = new \App\Models\Ingredient([
            'name' => "Tanqueray Rangpur",
            'quantity' => 50,
            'measurement_unit_id' => $mlId,
            'product_id' => 13,
            'product_diageo' => 'Y',
            'price_unit' => null,
            'country_id' => 1,
        ]);
        $ingredient->save();
        // Tanqueray & Tonic
        $ingredient = new \App\Models\Ingredient([
            'name' => "Agua Tonica",
            'quantity' => 100,
            'measurement_unit_id' => $mlId,
            'product_id' => 14,
            'price_unit' => 5.45,
            'country_id' => 1,
        ]);
        $ingredient->save();
        $ingredient = new \App\Models\Ingredient([
            'name' => "Naranja",
            'quantity' => 10,
            'measurement_unit_id' => $grId,
            'product_id' => 14,
            'price_unit' => 5,
            'country_id' => 1,
        ]);
        $ingredient->save();
        $ingredient = new \App\Models\Ingredient([
            'name' => "Tanqueray London Dry",
            'quantity' => 50,
            'measurement_unit_id' => $mlId,
            'product_id' => 14,
            'product_diageo' => 'Y',
            'price_unit' => null,
            'country_id' => 1,
        ]);
        $ingredient->save();
        // Bulleit & Beer
        $ingredient = new \App\Models\Ingredient([
            'name' => "Naranja",
            'quantity' => 10,
            'measurement_unit_id' => $grId,
            'product_id' => 15,
            'price_unit' => 5,
            'country_id' => 1,
        ]);
        $ingredient->save();
        $ingredient = new \App\Models\Ingredient([
            'name' => "Bulleit Bourbon",
            'quantity' => 30,
            'measurement_unit_id' => $mlId,
            'product_id' => 15,
            'product_diageo' => 'Y',
            'price_unit' => null,
            'country_id' => 1,
        ]);
        $ingredient->save();
        $ingredient = new \App\Models\Ingredient([
            'name' => "Cerveza Negra",
            'quantity' => 150,
            'measurement_unit_id' => $mlId,
            'product_id' => 15,
            'price_unit' => 9.09,
            'country_id' => 1,
        ]);
        $ingredient->save();
        // Singleton Penicilin
        $ingredient = new \App\Models\Ingredient([
            'name' => "Zumo de Limón",
            'quantity' => 15,
            'measurement_unit_id' => $mlId,
            'product_id' => 16,
            'price_unit' => 12,
            'country_id' => 1,
        ]);
        $ingredient->save();
        $ingredient = new \App\Models\Ingredient([
            'name' => "Singleton",
            'quantity' => 45,
            'measurement_unit_id' => $mlId,
            'product_id' => 16,
            'product_diageo' => 'Y',
            'price_unit' => null,
            'country_id' => 1,
        ]);
        $ingredient->save();
        $ingredient = new \App\Models\Ingredient([
            'name' => "Jengibre",
            'quantity' => 30,
            'measurement_unit_id' => $grId,
            'product_id' => 16,
            'price_unit' => 20,
            'country_id' => 1,
        ]);
        $ingredient->save();
        /*$ingredient = new \App\Models\Ingredient([
            'name' => "Jengibre",
            'quantity' => 30,
            'measurement_unit_id' => $grId,
            'product_id' => 16,
            'price_unit' => 20,
            'country_id' => 1,
        ]);
        $ingredient->save();*/
        $ingredient = new \App\Models\Ingredient([
            'name' => "Miel de Abeja",
            'quantity' => 10,
            'measurement_unit_id' => $grId,
            'product_id' => 16,
            'price_unit' => 40,
            'country_id' => 1,
        ]);
        $ingredient->save();
        // Trago Singleton
        $ingredient = new \App\Models\Ingredient([
            'name' => "Singleton",
            'quantity' => 50,
            'measurement_unit_id' => $mlId,
            'product_id' => 17,
            'product_diageo' => 'Y',
            'price_unit' => null,
            'country_id' => 1,
        ]);
        $ingredient->save();
        // Trago Buchanan's 18
        $ingredient = new \App\Models\Ingredient([
            'name' => "Trago Buchanan's 18",
            'quantity' => 50,
            'measurement_unit_id' => $mlId,
            'product_id' => 18,
            'product_diageo' => 'Y',
            'price_unit' => null,
            'country_id' => 1,
        ]);
        $ingredient->save();
        // Botll Old Parr 18
        $ingredient = new \App\Models\Ingredient([
            'name' => "Old Parr 18",
            'quantity' => 50,
            'measurement_unit_id' => $mlId,
            'product_id' => 19,
            'product_diageo' => 'Y',
            'price_unit' => null,
            'country_id' => 1,
        ]);
        $ingredient->save();


        $all = \App\Models\Ingredient::where("product_diageo",'=','N')->get();
        foreach ($all as $item) {
            $ingredient = \App\Models\Ingredient::find($item->id);
            $ingredient->purchase_price = 500 * $ingredient->price_unit;
            $ingredient->purchase_quantity = 500;
            $ingredient->save();
        }


    }
}

